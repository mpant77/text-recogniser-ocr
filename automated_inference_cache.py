# -*- coding: utf-8 -*-
"""
@author - Vaibhav

"""
from cv2 import imwrite
from datetime import datetime
import json
from math import frexp
import numpy as np
from os import getcwd, makedirs, walk, remove, rename
from os.path import isfile, exists, basename, join
from pandas import DataFrame, merge, read_csv, read_json
from random import shuffle
from shutil import copy, rmtree, move
from sys import stderr
from tempfile import mkdtemp

from common_utils import estimated_time_remaining, PROCESSED_EXTENSION, UNPROCESSED_EXTENSION, \
    output_columns, transform_predictions, merge_properties
from common_utils import get_inference_parameters, FileUtils, get_ftp_options, is_valid_file
from common_utils import get_remote_input_file, get_local_input_file, exist_ftp, send_daily_summary, generate_old_output_file
from common_utils import ImageUtils, get_run_state, unprocessed_set, get_chunks, FINISHED_EXTENSION, FINISHED_EXTENSION_old
import dbhandler
from deep_networks import DeepNetworks, ModelUtils
from logs_utils import init_logging, get_default_logger
from parallel_utils import ParallelProcess, ParallelJob
from remote_transfer import FTPTransfer
from StopWatch import StopWatch
from tf_session_manager import TFSessionManager
from tf_session_manager_inception import TFSessionManagerInception


tf_sess_OD = TFSessionManager("./tf_od_graph_faster/")
tf_sess_Construction = TFSessionManager("./tf_od_graph_construction/")
tf_sess_inception_dict={}

def load_tensorflow_models():
    global tf_sess_inception_dict
    files = FileUtils.get_files_by_extension(getcwd())
    params = get_inference_parameters(files['cfg'][0])
    tf_models = ['TF_main', 'TF_amenities', 'TF_bathroom', 'TF_bedroom', 'TF_floorplan', 'TF_exteriorothers', 'TF_water', 'TF_construction', 'TF_linedrawing']
    for model_name in tf_models:
        tf_sess_inception_dict[model_name] = [TFSessionManagerInception(params[model_name], model_name), params[model_name]]


load_tensorflow_models()


def get_prediction(model_name, batch, add_model_details=False):
    global tf_sess_inception_dict
    model = tf_sess_inception_dict[model_name][0]
    model_path = tf_sess_inception_dict[model_name][1].split('/')[-2]
    out_dict = {}
    for idx, row in batch.iterrows():
        if row['resize_image'] is not None:
            label, prob = model.run_single_session(row['resize_image'])
            out_dict[row['ImageName']] = [label, prob]
        else:
            out_dict[row['ImageName']] = []
    return DataFrame.from_dict(out_dict, orient='index').reset_index()


def generate_sublabels(predictions, batch, params):
    #other_categories = {'Floor Plan':'floorplan', 'Exterior Others': 'exteriorothers', 'WaterFeatures': 'water'}
    sub_categories = {'Amenities':'TF_amenities', 'Bathroom':'TF_bathroom', 'Bedroom':'TF_bedroom', 'Floor Plan':'TF_floorplan', 'Exterior Others':'TF_exteriorothers',
     'WaterFeatures':'TF_water'}
    result = [] 
    for index, row in predictions.iterrows():
        if row['Prob1'] > 0.75:
            if row['Label1'] in sub_categories:
                intermediate_batch = batch[batch['ImageName'] == row.ImageName]
                inter_predictions = get_prediction(sub_categories[row['Label1']], intermediate_batch)
                inter_predictions.columns = ['ImageName', 'Name', 'Prob']
                result.append(inter_predictions[['Name', 'Prob']].to_dict(orient='records'))
            else:
                result.append([])
        else:
            result.append([])
    return result


def update_flag_based_predictions(predictions, new_predictions, column_name, label_to_check):
    if len(predictions) > 0 and len(new_predictions) > 0:
        new_predictions[column_name] = new_predictions['Label1'].apply(lambda x: 1 if x == label_to_check else 0)
        return merge(predictions, new_predictions[['ImageName', column_name]], how='inner', on=['ImageName'])


def check_elevation_line_drawing(predictions):
    predictions['Label1'] = np.where((predictions['Label1'] == 'Elevation Front') & (predictions['IsLineDrawing']) , 'Elevation Line Drawing', predictions['Label1'])
    #predictions['Label2'] = np.where((predictions['Label2'] == 'Elevation Front') & (predictions['IsLineDrawing']) , 'Elevation Line Drawing', predictions['Label2'])
    predictions['Label1'] = np.where((predictions['Label1'] == 'TownHomes') & (predictions['IsLineDrawing']) , 'Elevation Line Drawing', predictions['Label1'])
    predictions['Label1'] = np.where((predictions['Label1'] == 'Greatroom') , 'Greatroom Combo', predictions['Label1'])
    #predictions['Label2'] = np.where((predictions['Label2'] == 'Greatroom') , 'Greatroom Combo', predictions['Label2'])
    return predictions


def process_predictions(params, batch, od_predictions):  # TODO _ Main
    predictions = get_prediction('TF_main', batch, add_model_details=True)
    predictions.columns = ['ImageName', 'Label1', 'Prob1']
    predictions = merge(predictions, batch[['ImageName', 'Height', 'Width']], how='left')
    
    drawing_predictions = get_prediction('TF_linedrawing', batch)
    drawing_predictions.columns = ['ImageName', 'Label1', 'Prob1']

    construction_predictions = get_prediction('TF_construction', batch)
    construction_predictions.columns = ['ImageName', 'Label1', 'Prob1']

    predictions['SubLabels'] = generate_sublabels(predictions, batch, params)
    predictions = update_flag_based_predictions(predictions, drawing_predictions, 'IsLineDrawing', 'Line Drawing')
    predictions = update_flag_based_predictions(predictions, construction_predictions, 'IsConstruction', 'construction')
    predictions = check_elevation_line_drawing(predictions)
    if len(predictions) > 0:
        #predictions['IsLineDrawing'] = 0
        return predictions
    else:
        return DataFrame()


def process_OD_predictions(batch, session_model, label, features=False):
    batch[label] = batch['image'].apply(lambda x: session_model.run_single_session(x, features))   
    return batch[['ImageName', label]]


def process_image_url(image_url, image_params):
    """
    Process a given image url to compute it's metadata and resize it for prediction

    Arguments:
    :param image_url: full image url
    :param image_params: all parameters related to image processing

    :return: a dictionary of image properties including name, url, resized image, metadata
    :type image_url: str
    :type image_params: dict
    """
    #status = 'OK'
    logger = get_default_logger()
    image_name = image_url.rsplit('/', 1)[-1]
    full_image_path = '/'.join([image_params['cache'], image_name])
    download_status, dl_status = ImageUtils.get_image_from_url(image_url, full_image_path)
    file_read_status = False
    original_image = None
    try:
        if download_status != 2:  # try to read only downloaded images
                original_image = ImageUtils.read_image(full_image_path)
                if original_image is not None:
                    file_read_status = True
                else:
                    status = 'Read Error'
                    logger.error('ImageClassifierError - Read Error,{},{}'.format(image_url, full_image_path))
        else:
            status = 'Download Error'
            logger.error('ImageClassifierError - Download Error {},{},{}'.format(dl_status,image_url, full_image_path))
    except Exception as ex:
        logger.critical(ex)
        logger.error('ImageClassifierError - Download/Read Error,{},{}'.format(image_url, full_image_path))
    metadata = None
    resize_image = None
    phash = None
    if file_read_status:
        use_scipy = False
        # if image_name.endswith(('gif', 'svg')):
        #     use_scipy = True
        metadata = ImageUtils.get_image_metadata(full_image_path)
        image = original_image.copy()
        if image_params['unsharp']:
            image = ImageUtils.sharpen_image(image)
        if image_params['equalize']:
            image = ImageUtils.image_histogram_equalization(image)
        resize_image = ImageUtils.resize_image(image, use_scipy, image_params['shape'], image_params['shape'])
        if resize_image is not None:
            if image_params['save']:
                resized_image_name = '/'.join([image_params['cache'], image_name.split('.')[0] + '-resize.jpg'])
                imwrite(resized_image_name, resize_image)
        else:
            status = 'Image Resize Error'
        phash = ImageUtils.get_imagehash(image)
    ret = {'ImageName': image_name, 'Url': image_url, 'resize_image': resize_image, 'Phash': phash, 'image': original_image}
    if metadata is not None:
        ret.update(metadata)
    else:
        ret.update(dict(Height=None, Width=None, PPIx=None, PPIy=None, Color=None, IsBlur=None, Fingerprint=None))
    #ret.update(dict(Status=status))
    return ret#, full_image_path


def cleanup(hostname, ftp_download_path, upload_status, params, error_file_name):
    if upload_status:
        files = FileUtils.get_files_by_extension(getcwd())
        if ftp_download_path is not None:
            modified_name = ftp_download_path + '/' + files['csv'][0].rsplit('/', 1)[-1] + '_process'
            try:
                ftp = FTPTransfer(hostname)
                ftp.do_login()
                ftp.rename(modified_name, modified_name + 'ed')
            except Exception as ex:
                logger = get_default_logger()
                logger.error(ex)
            finally:
                ftp.close()

        path_filename = files['csv'][0].rsplit('/', 1)
        output_root_path = path_filename[0] + '/output/'
        send_daily_summary(files['csv'][0], files[FINISHED_EXTENSION][0], params, ftp_download_path, error_file_name)
        move(files['csv'][0], output_root_path + path_filename[1])
        move(files[FINISHED_EXTENSION][0], output_root_path + files[FINISHED_EXTENSION][0].rsplit('/', 1)[-1])


def append_output(final, output_file_name):
    """
    Appends the given processed records to output file

    Arguments:
    :param final: final output records
    :param output_file_name: output file name for processed records

    :return: None
    :type final: DataFrame
    :type output_file_name: str
    """
    if final.size > 0:
        if not isfile(output_file_name):
            DataFrame(columns=output_columns()).to_csv(output_file_name, index=False, sep='|')
        with open(output_file_name, 'a') as output_stream:
            final.to_csv(output_stream, index=False, header=False, sep='|')
            output_stream.flush()


def process_files_cleanup():
    """
    Clean up the temporary working files and create the final output file
    :return: None
    """
    files = FileUtils.get_files_by_extension(getcwd())
    # remove all unprocessed
    if UNPROCESSED_EXTENSION in files and len(files[UNPROCESSED_EXTENSION]) > 0:
        for unprocessed in files[UNPROCESSED_EXTENSION]:
            if isfile(unprocessed):
                remove(unprocessed)

    if FINISHED_EXTENSION not in files:
        output_file_name = FileUtils.get_modified_name(files['csv'][0]) + '.' + FINISHED_EXTENSION
        if PROCESSED_EXTENSION in files:
            rename(files[PROCESSED_EXTENSION][0], output_file_name)


def download_input_file(hostname, remote_dir):
    """
    Find and download input csv file from remote_dir on hostname

    Arguments:
    :param hostname: ftp hostname
    :param remote_dir: full path to remote directory on hostname

    :return: download status of input file
    :type hostname: str
    :type remote_dir: str
    """
    logger = get_default_logger()
    ftp = FTPTransfer(hostname)
    ftp.do_login()
    files = FileUtils.index_files_by_extension(ftp.list_directory(remote_dir))
    selected_file_name = get_local_input_file()
    download_ftp = False
    download_status = 2
    if selected_file_name != '':
        download_ftp = not (exist_ftp(selected_file_name, files))
    else:
        download_ftp = True

    if download_ftp:
        selected_file_name = get_remote_input_file(ftp, remote_dir)
        if selected_file_name != '':
            local_file_name = getcwd() + '/' + selected_file_name
            remote_file_name = remote_dir + '/' + selected_file_name
            download_status = 0
            try:
                if not is_valid_file(local_file_name):
                    download_status = ftp.download_file(remote_file_name, local_file_name)
                else:
                    logger.info('Remote file already exists at %s' % local_file_name)
                    download_status = 2
                if download_status == 1:
                    logger.info('Downloaded input file %s from %s' % (selected_file_name, '/'.join([hostname, remote_dir])))
                    ftp.rename(remote_file_name, remote_file_name + '_process')
            except Exception as ex:
                logger.error(ex.message)
                # remove only empty file
                if not is_valid_file(local_file_name):
                    remove(local_file_name)
            finally:
                ftp.close()

            if download_status == 0:  # remove local file if ftp download fails
                if isfile(local_file_name):
                    remove(local_file_name)

    return download_status


def merge_manual_qa_data():
    logger = get_default_logger()
    files = FileUtils.get_files_by_extension(getcwd())
    local_file_name = files[FINISHED_EXTENSION][0].rsplit('/', 1)[-1]
    processed_data = read_csv(local_file_name, sep='|')
    columns_order = processed_data.columns
    manual_qa_data = read_csv('manual_qa.csv_qa')
    processed_same_data = processed_data[processed_data.Fingerprint.isin(manual_qa_data.Fingerprint)]
    processed_data = processed_data[~processed_data.Fingerprint.isin(manual_qa_data.Fingerprint)]
    del processed_same_data['Category']
    processed_same_data = merge(processed_same_data, manual_qa_data, how='left', on='Fingerprint')
    processed_same_data['IsVerified'] = 1
    processed_data = processed_data.append(processed_same_data, sort=True)
    processed_data.to_csv(local_file_name, columns=columns_order, index=False, sep='|')


def upload_old_output_file(hostname, remote_dir):
    """
    Finds finished output file and uploads it to remote_dir on hostname

    Arguments:
    :param hostname: ftp hostname
    :param remote_dir: full path to remote directory on hostname

    :return: upload status of output file
    :type hostname: str
    :type remote_dir: str
    """
    logger = get_default_logger()
    files = FileUtils.get_files_by_extension(getcwd())
    local_file_name = files[FINISHED_EXTENSION][0].rsplit('/', 1)[-1]
    copy_file_name = local_file_name.split('.', 1)[0] + '.final2'
    generate_old_output_file(local_file_name, copy_file_name)
    files = FileUtils.get_files_by_extension(getcwd())
    local_file_name = files[FINISHED_EXTENSION_old][0].rsplit('/', 1)[-1]
    copy_file_name = local_file_name.split('.', 1)[0] + '.new_csv'
    if not is_valid_file(copy_file_name):
        copy(files[FINISHED_EXTENSION_old][0], copy_file_name)

    ftp = FTPTransfer(hostname)
    ftp.do_login()
    upload_status = 0
    try:
        upload_status = ftp.upload_file(remote_dir, copy_file_name)
    except Exception as ex:
        print(ex)
        logger.error(ex)
    finally:
        ftp.close()
    if upload_status:
        logger.info('Output file uploaded at %s ' % remote_dir + '/' + copy_file_name)
        remove(copy_file_name)

    return upload_status


def upload_output_file(hostname, remote_dir):
    """
    Finds finished output file and uploads it to remote_dir on hostname

    Arguments:
    :param hostname: ftp hostname
    :param remote_dir: full path to remote directory on hostname

    :return: upload status of output file
    :type hostname: str
    :type remote_dir: str
    """
    logger = get_default_logger()
    files = FileUtils.get_files_by_extension(getcwd())
    local_file_name = files[FINISHED_EXTENSION][0].rsplit('/', 1)[-1]
    copy_file_name = local_file_name.split('.', 1)[0] + '.newod_csv'
    if not is_valid_file(copy_file_name):
        copy(files[FINISHED_EXTENSION][0], copy_file_name)
    upload_old_output_file(hostname, remote_dir)
    ftp = FTPTransfer(hostname)
    ftp.do_login()
    upload_status = 0
    try:
        upload_status = ftp.upload_file(remote_dir, copy_file_name, True)
    except Exception as ex:
        print(ex)
        logger.error(ex)
    finally:
        ftp.close()
    if upload_status:
        logger.info('Output file uploaded at %s ' % remote_dir + '/' + copy_file_name)
        remove(copy_file_name)

    return upload_status


def process_records(unprocessed, params, output_file_name, records_batch_size=0):
    """
    Process the given set of records to obtain image properties

    Arguments:
    :param unprocessed: total number of records that are not processed
    :param params:
    :param output_file_name: output file name for processed records

    Keyword arguments:
    :param records_batch_size: number of records to process in parallel

    :return: None
    :type unprocessed: DataFrame
    :type params: dict
    :type output_file_name: str
    :type records_batch_size: int
    """
    logger = get_default_logger()
    if records_batch_size == 0:
        records_batch_size = params['batch']
    num_consumers = frexp(records_batch_size / 10)[1]
    url_col = params['cols'][1]
    t = StopWatch()
    processing_time = 0.0
    done = 0
    if not params['use-cache']:
        params['cache'] = mkdtemp()
    if not exists(params['cache']):
        makedirs(params['cache'])
    unique_unprocessed = unprocessed.drop_duplicates(subset=url_col)
    total_images = len(unique_unprocessed)
    cnt = 1
    with ParallelProcess(num_consumers) as p:
        for records in get_chunks(records_batch_size, unique_unprocessed):
            logger.info('Processing batch %d' % cnt)
            records = records.dropna(axis=0)
            t.tic()
            p.enqueue_jobs(ParallelJob, process_image_url, records[url_col].drop_duplicates(), params)
            results = DataFrame(p.output())
            p.reset()
            results = results.dropna(axis=0)  # drop rows with any column value as None
            t.toc()
            dtime = t.time
            res_len = len(results)
            if res_len > 0:
                msg = 'Total %d images download and metadata obtained in %s ' % (res_len, StopWatch.to_string(dtime))
                logger.info(msg)
                t.tic()
                for batch in get_chunks(params['batch'], results):
                    batch = batch.dropna(axis=0)
                    od_predictions = process_OD_predictions(batch[['ImageName', 'image']], tf_sess_OD, 'ObjectDetection', True)
                    construction_predictions = process_OD_predictions(batch[['ImageName', 'image']], tf_sess_Construction, 'ConstructionObjects')
                    predictions = process_predictions(params, batch[['ImageName', 'resize_image', 'Height', 'Width']], od_predictions)
                    if len(predictions) > 0:
                        predictions = merge(predictions, od_predictions, how='left')#.dropna(axis=0)
                        predictions = merge(predictions, construction_predictions, how='left')#.dropna(axis=0)
                        predictions['Generation'] = params['generation']
                        original_records = merge(unprocessed, records, how='left').dropna(axis=0)
                        final = merge_properties(predictions, original_records, batch, params['cache'])
                        append_output(final, output_file_name)
                t.toc()
                logger.info('Total %d images prediction obtained in %s ' % (res_len, StopWatch.to_string(t.time)))
            done += res_len
            ptime = t.time
            total_batch_time = dtime + ptime
            processing_time += total_batch_time
            remaining_images = total_images - cnt * records_batch_size
            rem = StopWatch.to_string(estimated_time_remaining(len(records), total_batch_time, remaining_images))
            if remaining_images < 1:
                remaining_images = 0
                rem = '0 seconds'
            if res_len > 0:
                msg = 'Total %d records processed in %s remaining %d Estimated time remaining %s' % \
                      (done, StopWatch.to_string(processing_time), remaining_images, rem)
                logger.info(msg)
            else:
                logger.info('No records processed for batch {:d} remaining records {:d}'.format(cnt, remaining_images))
            cnt += 1

    # clean up temporary cache directory
    if not params['use-cache']:
        rmtree(params['cache'])


@StopWatch.timeit
def main(hostname=None, ftp_download_path=None, ftp_upload_path=None):
    """
    Main Driver program for running automated inference job
    :return: None
    """
    if not exists('logs'):
        makedirs('logs')
    logger, error_file_name = init_logging()
    print(hostname, ftp_download_path, ftp_upload_path)
    if hostname is not None and ftp_download_path is not None:
        download_input_file(hostname, ftp_download_path)
    else:
        logger.debug('Download of input file from ftp disabled.')
        logger.debug('Hostname provided %s and download path provided %s ' % (hostname, ftp_download_path))
    files = FileUtils.get_files_by_extension(getcwd())
    params = get_inference_parameters(files['cfg'][0])
    params['input-file'] = basename(files['csv'][0])
    if not exists('output'):
        makedirs('output')
    run_state = get_run_state(files)
    if run_state < 3:
        unprocessed = unprocessed_set(files, run_state, params['cols'])
        if run_state == 2:
            output_file_name = files[PROCESSED_EXTENSION][0]
        else:
            output_file_name = FileUtils.get_modified_name(files['csv'][0]) + '.' + PROCESSED_EXTENSION

        logger.debug('Automated inference job started ' + FileUtils.get_modified_name(params['input-file']))
        process_records(unprocessed, params, output_file_name, params['download-batch'])
    elif run_state == 4:
        logger.debug('All records have been processed. Please look for output file ending with %s ' % FINISHED_EXTENSION)
    else:
        logger.debug('Examine the unprocessed file to notice if processed already finished but failed during cleanup')
    process_files_cleanup()
    merge_manual_qa_data()
    upload_status = False
    if hostname is not None and ftp_upload_path is not None:
        upload_status = upload_output_file(hostname, ftp_upload_path)
    else:
        logger.debug('Upload of output file to ftp disabled.')
        logger.debug('Hostname provided %s and upload path provided %s ' % (hostname, ftp_upload_path))
    cleanup(hostname, ftp_download_path, upload_status, params, error_file_name)


if __name__ == "__main__":
    ftp_parser = get_ftp_options()
    args = ftp_parser.parse_args()
    main(args.host, args.down, args.up)