import logging
import logging.config
from datetime import datetime


def get_default_log_config():
    """
    Obtain default log configuration with following configuration
    Console messages at DEBUG level
    Separate logs for error and info messages
    UTC time stamped log file names stored under logs directory

    :return: logs configuration dictionary
    """
    time_stamp = datetime.utcnow().strftime("%b-%d-%YT%M-%S")
    config = dict(version=1, disable_existing_loggers=False, formatters={
        'simple': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        }
    }, handlers={
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'simple',
            'stream': 'ext://sys.stdout'
        },

        'info_file_handler': {
            'class': 'logging.FileHandler',
            'level': 'INFO',
            'formatter': 'simple',
            'filename': 'logs/' + time_stamp + '-info-image-classification.log',
            'encoding': 'utf8',
            'delay': True
        },

        'error_file_handler': {
            'class': 'logging.FileHandler',
            'level': 'ERROR',
            'formatter': 'simple',
            'filename': 'logs/' + time_stamp + '-errors-image-classification.log',
            'encoding': 'utf8',
            'delay': True
        }
    }, loggers={
        'my_module': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': 'no'
        }
    }, root={
        'level': 'DEBUG',
        'handlers': ['console', 'info_file_handler', 'error_file_handler']
    })
    return config


def get_default_logger(name='image_classification'):
    """
    Obtain instance of default logger

    Keyword arguments:
    :param name: common application name for logger

    :return: logger object
    :type name: str
    """
    return logging.getLogger(name)


def init_logging(name='image_classification', config=get_default_log_config()):
    """
    Instantiates logger configuration with default settings
    and returns an instance of logger

    Keyword arguments:
    :param name: common application name for logger
    :param config: configuration options for logs

    :return: logger object
    :type name: str
    :type config: dict
    """

    logger = get_default_logger(name)
    logging.config.dictConfig(config)
    return logger, "./" + config['handlers']['error_file_handler']['filename']
