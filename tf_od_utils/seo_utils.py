import numpy as np

converted_labels_dict = {
    'Ac Vent':'appliance.air-ventilation',
    'Coffee Maker':'appliance.coffee-maker',
    'Cooktop':'appliance.cook-top',
    'Dish Washer.black':'appliance.dishwasher.black',
    'Dish Washer.coloured':'appliance.dishwasher.colored',
    'Dish Washer.stainless-steel':'appliance.dishwasher.stainless-steel',
    'Dish Washer.white':'appliance.dishwasher.white',
    'Microwave.black':'appliance.microwave.black',
    'Microwave.coloured':'appliance.microwave.colored',
    'Microwave.stainless-steel':'appliance.microwave.stainless-steel',
    'Microwave.white':'appliance.microwave.white',
    'Oven.black':'appliance.oven.black',
    'Oven.coloured':'appliance.oven.colored',
    'Oven.stainless-steel':'appliance.oven.stainless-steel',
    'Oven.white':'appliance.oven.white',
    'Chimney':'appliance.range-hood',
    'Range.black':'appliance.range.black',
    'Range.coloured':'appliance.range.colored',
    'Range.stainless-steel':'appliance.range.stainless-steel',
    'Range.white':'appliance.range.white',
    'Refrigerator.black':'appliance.refrigerator.black',
    'Refrigerator.coloured':'appliance.refrigerator.colored',
    'Refrigerator.stainless-steel':'appliance.refrigerator.stainless-steel',
    'Refrigerator.white':'appliance.refrigerator.white',
    'Telephone':'appliance.telephone',
    'Toaster':'appliance.toaster',
    'Water Dispenser':'appliance.water-dispenser',
    'Bottle':'bar-wine.bottle',
    'Glass':'bar-wine.wine-glass',
    'Wine Holder':'bar-wine.wine-holder',
    'Wine Rack':'bar-wine.wine-rack',
    'Bath Towel':'bath-accessory.bath-towel',
    'Soap Dish':'bath-accessory.soap-dish',
    'Soap Dispenser':'bath-accessory.soap-dispenser',
    'Toothbrush Holder':'bath-accessory.toothbrush-holder',
    'Towel Holder':'bath-accessory.towel-holder',
    'Pillow':'bedding.pillow',
    'Sofa Blankets':'bedding.sofa-blanket',
    'Sofa Blanket':'bedding.sofa-blanket',
    'cabinet.glass-front.beige':'cabinet.glass-front.beige',
    'cabinet.glass-front.black':'cabinet.glass-front.black',
    'cabinet.glass-front.blue':'cabinet.glass-front.blue',
    'cabinet.glass-front.grey':'cabinet.glass-front.grey',
    'cabinet.glass-front.olive':'cabinet.glass-front.olive',
    'cabinet.glass-front.red':'cabinet.glass-front.red',
    'cabinet.glass-front.white':'cabinet.glass-front.white',
    'cabinet.glass-front.wood-cherry':'cabinet.glass-front.wood-cherry',
    'cabinet.glass-front.wood-dark':'cabinet.glass-front.wood-dark',
    'cabinet.glass-front.wood-light':'cabinet.glass-front.wood-light',
    'cabinet.glass-front.wood-medium':'cabinet.glass-front.wood-medium',
    'cabinet.open.beige':'cabinet.open.beige',
    'cabinet.open.black':'cabinet.open.black',
    'cabinet.open.grey':'cabinet.open.grey',
    'cabinet.open.olive':'cabinet.open.olive',
    'cabinet.open.white':'cabinet.open.white',
    'cabinet.open.wood-dark':'cabinet.open.wood-dark',
    'cabinet.open.wood-light':'cabinet.open.wood-light',
    'cabinet.open.wood-medium':'cabinet.open.wood-medium',
    'cabinet.raised-panel.beige':'cabinet.raised-panel.beige',
    'cabinet.raised-panel.black':'cabinet.raised-panel.black',
    'cabinet.raised-panel.grey':'cabinet.raised-panel.grey',
    'cabinet.raised-panel.olive':'cabinet.raised-panel.olive',
    'cabinet.raised-panel.white':'cabinet.raised-panel.white',
    'cabinet.raised-panel.wood-cherry':'cabinet.raised-panel.wood-cherry',
    'cabinet.raised-panel.wood-dark':'cabinet.raised-panel.wood-dark',
    'cabinet.raised-panel.wood-light':'cabinet.raised-panel.wood-light',
    'cabinet.raised-panel.wood-medium':'cabinet.raised-panel.wood-medium',
    'cabinet.recessed-panel.beige':'cabinet.recessed-panel.beige',
    'cabinet.recessed-panel.black':'cabinet.recessed-panel.black',
    'cabinet.recessed-panel.grey':'cabinet.recessed-panel.grey',
    'cabinet.recessed-panel.olive':'cabinet.recessed-panel.olive',
    'cabinet.recessed-panel.white':'cabinet.recessed-panel.white',
    'cabinet.recessed-panel.wood-cherry':'cabinet.recessed-panel.wood-cherry',
    'cabinet.recessed-panel.wood-dark':'cabinet.recessed-panel.wood-dark',
    'cabinet.recessed-panel.wood-light':'cabinet.recessed-panel.wood-light',
    'cabinet.recessed-panel.wood-medium':'cabinet.recessed-panel.wood-medium',
    'cabinet.recessed-panel.wood-red':'cabinet.recessed-panel.wood-red',
    'cabinet.shaker.beige':'cabinet.shaker.beige',
    'cabinet.shaker.black':'cabinet.shaker.black',
    'cabinet.shaker.blue':'cabinet.shaker.blue',
    'cabinet.shaker.brown':'cabinet.shaker.brown',
    'cabinet.shaker.green':'cabinet.shaker.green',
    'cabinet.shaker.grey':'cabinet.shaker.grey',
    'cabinet.shaker.olive':'cabinet.shaker.olive',
    'cabinet.shaker.white':'cabinet.shaker.white',
    'cabinet.shaker.wood-cherry':'cabinet.shaker.wood-cherry',
    'cabinet.shaker.wood-dark':'cabinet.shaker.wood-dark',
    'cabinet.shaker.wood-light':'cabinet.shaker.wood-light',
    'cabinet.shaker.wood-medium':'cabinet.shaker.wood-medium',
    'cabinet.slab.beige':'cabinet.slab.beige',
    'cabinet.slab.black':'cabinet.slab.black',
    'cabinet.slab.blue':'cabinet.slab.blue',
    'cabinet.slab.brown':'cabinet.slab.brown',
    'cabinet.slab.cyan':'cabinet.slab.cyan',
    'cabinet.slab.grey':'cabinet.slab.grey',
    'cabinet.slab.navy':'cabinet.slab.navy',
    'cabinet.slab.olive':'cabinet.slab.olive',
    'cabinet.slab.pink':'cabinet.slab.pink',
    'cabinet.slab.red':'cabinet.slab.red',
    'cabinet.slab.white':'cabinet.slab.white',
    'cabinet.slab.wood-cherry':'cabinet.slab.wood-cherry',
    'cabinet.slab.wood-dark':'cabinet.slab.wood-dark',
    'cabinet.slab.wood-light':'cabinet.slab.wood-light',
    'cabinet.slab.wood-medium':'cabinet.slab.wood-medium',
    'Kettle':'cookware.kettle',
    'Countertop.Granite':'int.counter-top.granite',
    'Countertop.Laminate':'int.counter-top.laminate',
    'Countertop.Marble':'int.counter-top.marble',
    'Countertop.Plain':'int.counter-top.plain',
    'Countertop.Wood':'int.counter-top.wood',
    'Basket':'decor.basket',
    'Candle Stand':'decor.candle-holder',
    'Clock':'decor.clock',
    'Decorative Item':'decor.decorative-item',
    'Mirror':'decor.mirror',
    'Painting':'decor.painting',
    'Photo Frame':'decor.photo-frame',
    'Planter':'decor.planter',
    'Poster':'decor.poster',
    'Table Clock':'decor.table-clock',
    'Table Mat':'decor.table-mat',
    'Toys':'decor.toys',
    'Trophy':'decor.trophy',
    'Vase':'decor.vase',
    'Decorative Wall Items':'decor.wall-decor',
    'Blinds':'window-treatment.blinds',
    'Curtains':'window-treatment.curtain-drape',
    'Valance':'window-treatment.valance',
    'Sidelite':'door.sidelites',
    'Ceiling Fan':'electrical.ceiling-fan.ceiling-fan',
    'Ceiling Fan Light':'electrical.ceiling-fan.ceiling-fan-light',
    'Ceiling Speakers':'electrical.ceiling-speaker',
    'Laptop':'electrical.laptop',
    'Projector':'electrical.projector',
    'Speakers':'electrical.speaker',
    'TV':'electrical.tv',
    'Projector Screen':'electrical.projector-screen',
    'Wall Speakers':'electrical.wall-speaker',
    'Fireplace Outlet':'ext.chimney',
    'Chimney Outlet':'ext.chimney',
    'Round Stone Fire Pit':'ext.fire-pit',
    'Pond':'ext.pond',
    'Trellis':'ext.trellises',
    'Bathtub Faucet':'faucet.bathtub-faucet',
    'Faucet':'faucet.kitchen-faucet',
    'Mixer Tap':'faucet.mixer-tap',
    'floor.carpet.beige':'floor.carpet.beige',
    'floor.carpet.brown':'floor.carpet.brown',
    'floor.carpet.gray':'floor.carpet.gray',
    'floor.carpet.patterned':'floor.carpet.patterned',
    'floor.tile.ceramic':'floor.tile.ceramic',
    'floor.tile.limestone':'floor.tile.limestone',
    'floor.tile.marble':'floor.tile.marble',
    'floor.tile.stone':'floor.tile.stone',
    'floor.wooden.dark':'floor.wooden.dark',
    'floor.wooden.light':'floor.wooden.light',
    'floor.wooden.medium':'floor.wooden.medium',
    'Bean Bag':'furniture.bean-bag',
    'Bed Tray Table':'furniture.bed-tray-table',
    'Double Bed':'furniture.bed.double-bed',
    'Four Poster Bed':'furniture.bed.four-poster-bed',
    'Single Bed':'furniture.bed.single-bed',
    'Toddler Bed':'furniture.bed.toddler-bed',
    'Bedroom Bench':'furniture.bedroom-bench',
    'Bedside Table':'furniture.bedside-table',
    'Center Table':'furniture.center-table',
    'Chair':'furniture.chair.chair',
    'Papasan Chair':'furniture.chair.papasan-chair',
    'Chaise Lounge':'furniture.chaise-lounge',
    'Drawers':'furniture.drawers',
    'Dressing Table':'furniture.dressing-table',
    'TV Cabinet':'furniture.entertainment-center',
    'Ottoman':'furniture.ottoman',
    'Shelves':'furniture.shelf',
    'Side Table':'furniture.side-table',
    'Sofa Single':'furniture.sofa',
    'Sofa Triple':'furniture.sofa',
    'Sofa Double':'furniture.sofa',
    'Sofa Four':'furniture.sofa',
    'Sofa Bench':'furniture.sofa-bench',
    'Stools':'furniture.stool',
    'Console Table':'furniture.table.console-table',
    'Dining Table':'furniture.table.dining-table',
    'Wall Shelf':'furniture.wall-shelf',
    'Wardrobe':'furniture.wardrobe',
    'Air Hockey Table':'game.air-hockey-table',
    'Arcade Cabinet':'game.arcade-cabinet',
    'Casino Table':'game.casino-table',
    'Dartboard':'game.dartboard',
    'Foosball Table':'game.fusball-table',
    'Guitar':'game.guitar',
    'Keyboard':'game.keyboard',
    'Piano':'game.piano',
    'Billiards Table':'game.pool-table',
    'Shuffleboard':'game.shuffle-board',
    'Skee-Ball Machine':'game.skee-ball-machine',
    'TT Table':'game.table-tennis',
    'Tictactoe Board':'game.tictactoe-board',
    'Bath Tub':'int.bathtub',
    'Desk':'int.desk',
    'Glass Door':'door.glass',
    'Wooden Door':'door.wood',
    'Fireplace':'int.fireplace',
    'Kitchen Island':'int.kitchen-island',
    'Shower Area':'int.shower',
    'Sink Bathroom':'int.sink',
    'Sink Kitchen':'int.sink',
    'WC Seat':'int.toilet',
    'TV Unit':'int.tv-stand',
    'Projector Cabinet':'int.tv-stand',
    'Window':'int.window',
    'Juicer Blender':'appliance.juicer-blender',
    'Study Lamp':'lamp.desk',
    'Floor Lamp':'lamp.floor',
    'Table Lamp':'lamp.table',
    'Ceiling Mounted Light':'lighting.ceiling-light',
    'Ceiling Spotlight':'lighting.ceiling-spotlight',
    'Chandelier':'lighting.chandelier',
    'Pendant Light':'lighting.pendent',
    'Pendent Light':'lighting.pendent',
    'Recessed Light':'lighting.recessed',
    'Wall Lights':'lighting.wall-mounted',
    'Hand Shower':'shower.hand-shower',
    'Shower Cum Tub':'shower.shower-bathtub-combination',
    'Shower Head':'shower.shower-head',
    'Shower Mixer':'shower.shower-mixer',
    'Recliner Sofa':'sofa.recliner-sofa',
    'Sectional Sofa':'sofa.sectional-sofa',
    'Bowls':'tableware.bowl',
    'Crockery Set':'tableware.crockery-set',
    'Cups':'tableware.cup',
    'Chopping Board':'tableware.cutting-board',
    'Fruit Veg Tray':'tableware.fruit-veg-tray',
    'Jars':'tableware.jar',
    'Jug':'tableware.jug',
    'Plates':'tableware.plate',
    'Tray':'tableware.serving-tray'
}


def process(
        classes,
        scores,
        category_index,
        min_score_thresh=0.3):
    cabinet_style = {
        'raised-panel': 0,
        'recessed-panel': 0,
        'shaker': 0,
        'slab': 0,
        'glass-front': 0,
        'open': 0,
    }

    cabinet_finish = {
        'wood-dark': 0,
        'wood-medium': 0,
        'wood-light': 0,
        'wood-cherry': 0,
        'black': 0,
        'white': 0,
        'beige': 0,
        'grey': 0,
    }

    floor_style = {
        'carpet.beige': 0,
        'carpet.brown': 0,
        'carpet.gray': 0,
        'carpet.patterned': 0,
        'tile.ceramic': 0,
        'tile.limestone': 0,
        'tile.marble': 0,
        'tile.stone': 0,
        'wooden.dark': 0,
        'wooden.light': 0,
        'wooden.medium': 0,
        'cement.gray': 0,
    }

    appliances_finish = {
        'stainless-steel': 0,
        'black': 0,
        'white': 0
    }

    countertop_type = {
        'granite': 0,
        'marble': 0,
        'plain': 0,
        'laminate': 0,
        'wooden': 0,
    }

    appliances = ['Refrigerator', 'Microwave', 'Range']
    classes.shape = (-1, 1)
    scores.shape = (-1, 1)

    total_boxes = classes.shape[0]

    for index in range(total_boxes):
        score = scores[index][0]
        if score < min_score_thresh: continue
        class_index = classes[index][0]
        if class_index in category_index.keys():
            class_name = category_index[class_index]['name']
        else:
            class_name = 'N/A'
        major_class = class_name.split('.')[0]
        if major_class in appliances:
            appliances_color = class_name.split('.')[1]
            if appliances_color in appliances_finish:
                appliances_finish[appliances_color] = appliances_finish[appliances_color] + score

        if class_name.startswith('cabinet'):
            _, cab_style, cab_finish = class_name.split('.')
            if cab_style in cabinet_style:
                cabinet_style[cab_style] = (cabinet_style[cab_style] + score)
            if cab_finish in cabinet_finish:
                cabinet_finish[cab_finish] = (cabinet_finish[cab_finish] + score)

        if class_name.startswith('floor.'):
            _, flr_style, flr_finish = class_name.split('.')
            flr_style = ".".join([flr_style, flr_finish])
            if flr_style in floor_style:
                floor_style[flr_style] = (floor_style[flr_style] + score)

        if class_name.startswith('Countertop'):
            countertop = class_name.split('.')[1].lower()
            countertop_type[countertop] = (countertop_type[countertop] + score)

    seo_list = dict()
    seo_list['cabinet_style'] = []
    if cabinet_style['glass-front'] > 0.15:
        seo_list['cabinet_style'].append('glass-front')
        cabinet_style.pop('glass-front')
    if cabinet_style['open'] > 0.30:
        seo_list['cabinet_style'].append('open')
        cabinet_style.pop('open')
    cab_style = max(cabinet_style, key=cabinet_style.get)
    if cabinet_style[cab_style] > 0.25:
        seo_list['cabinet_style'].append(cab_style)

    cab_finish = max(cabinet_finish, key=cabinet_finish.get)
    if cabinet_finish[cab_finish] > 0.25:
        seo_list['cabinet_finish'] = cab_finish

    if len(seo_list['cabinet_style']) == 0:
        seo_list.pop('cabinet_style')

    if appliances_finish['black'] > 0.25:
        seo_list['appliance_finish'] = 'black'
    elif appliances_finish['white'] > 0.25:
        seo_list['appliance_finish'] = 'white'
    elif appliances_finish['stainless-steel'] > 0.3:
        seo_list['appliance_finish'] = 'stainless-steel'

    flr_style_final = max(floor_style, key=floor_style.get)
    if floor_style[flr_style_final] > 0.3:
        seo_list['floor_type'] = flr_style_final.split('.')[0]
        seo_list['floor_finish'] = flr_style_final.split('.')[1]

    countertop_final = max(countertop_type, key=countertop_type.get)
    if countertop_type[countertop_final] > 0.3:
        seo_list['countertop_type'] = countertop_final

    return seo_list


def accumulate_od_output(
        classes,
        scores,
        category_index,
        min_score_thresh=0.3):
    classes.shape = (-1, 1)
    scores.shape = (-1, 1)



    total_boxes = classes.shape[0]
    merged_output_dict = {}
    for index in range(total_boxes):
        score = scores[index][0]
        if score < min_score_thresh: continue
        class_index = classes[index][0]
        if class_index in category_index.keys():
            class_name = category_index[class_index]['name']
        else:
            class_name = 'N/A'

        if class_name in converted_labels_dict.keys():
            class_name_new = converted_labels_dict[class_name]
            tokens = class_name_new.split('.')
            main_class = tokens[0]
            sub_class = ".".join(tokens[1:3])
            if not main_class in merged_output_dict.keys():
                merged_output_dict[main_class] = []
            if len(tokens) == 3:
                sub_class_temp, class_type = tokens[1:3]
                items_in_class = merged_output_dict[main_class]
                for n in items_in_class:
                    if n.split('.')[0] == sub_class_temp:
                        continue
                if sub_class not in merged_output_dict[main_class]:
                    merged_output_dict[main_class].append(sub_class)
            elif len(tokens) == 2:
                if sub_class not in merged_output_dict[main_class]:
                    merged_output_dict[main_class].append(sub_class)

    print(merged_output_dict)
    return





