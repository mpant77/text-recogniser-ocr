import cv2
import numpy as np
import os
import tensorflow as tf
import time
import pytesseract
import concurrent.futures
import json

from fuzzywuzzy import fuzz
from fuzzywuzzy import process

from PIL import Image
from tf_od_utils import label_map_util
from tf_od_utils import visualization_utils as vis_util


class SessionManagerOCRFP:
    """ TFSessionManager class handles loading a pretrained model as a servable
      and storing the session as a static class variable which can be run whenever
      a request hits the endpoint.

      Arguments:
        model_dir: The path to directory where the frozen_inference_graph.pb and the labels file is located.
      """

    taxonomy_dict = None
    text_dictionary = None
    floor_level_dict = None
    ocr_unmatch_word_log_path = './fp_ocr_data/log-unmatched-word.txt'  # Path to log all unmatched words

    def __init__(self, model_dir):
        self.model_dir = model_dir
        self.__sess = None
        self.__graph = None
        self.__image_tensor = None
        self.__tensor_dict = None
        self.__category_index = None
        self.__min_confidence_text_detection = 0.8
        self.__bbox_enlarge_ratio = 0.1
        self.num_classes = 1
        self.initialize()
        self.image_output_path = './static/images/Output_OCR_FP'    # Path to save all image outputs
        fp_text_list_file_path = './fp_ocr_data/floor-plan-word-dict.txt'   # Path to file containing floor-plan words
        inscene_cat_dict_file_path = './fp_ocr_data/inscene-taxonomy-dict.txt'  # Path to dictionary of inscene categories
        floor_level_dict_file_path = './fp_ocr_data/floor-plan-level-dict.txt'  # Path to dictionary of floor level dictionary
        SessionManagerOCRFP.text_dictionary = SessionManagerOCRFP.generate_text_dictionary(fp_text_list_file_path)
        SessionManagerOCRFP.taxonomy_dict = SessionManagerOCRFP.generate_taxonomy_dict(inscene_cat_dict_file_path)
        SessionManagerOCRFP.floor_level_dict = SessionManagerOCRFP.generate_taxonomy_dict(floor_level_dict_file_path)

    def initialize(self):
        path_to_ckpt = os.path.join(self.model_dir, 'frozen_inference_graph.pb')
        path_to_labels = os.path.join(self.model_dir, 'object-detection.pbtxt')
        num_classes = self.num_classes
        label_map = label_map_util.load_labelmap(path_to_labels)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=num_classes,
                                                                    use_display_name=True)
        self.__category_index = label_map_util.create_category_index(categories)
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(path_to_ckpt, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
                # print "Model Successfully loaded from", TFSessionManager.model_dir
                config = tf.ConfigProto(allow_soft_placement=True)
                config.gpu_options.per_process_gpu_memory_fraction = 0.5
                sess = tf.Session(config=config)
                ops = detection_graph.get_operations()
                all_tensor_names = {output.name for op in ops for output in op.outputs}
                tensor_dict = {}
                for key in [
                    'num_detections', 'detection_boxes', 'detection_scores',
                    'detection_classes', 'detection_masks'
                ]:
                    tensor_name = key + ':0'
                    if tensor_name in all_tensor_names:
                        tensor_dict[key] = detection_graph.get_tensor_by_name(
                            tensor_name)
                self.__image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                self.__graph = detection_graph
                self.__sess = sess
                self.__tensor_dict = tensor_dict

    @staticmethod
    def generate_text_dictionary(list_file_path):
        # Load words from dictionary
        fp_dictionary = set()
        with open(list_file_path, 'r') as f:
            for line in f.readlines():
                fp_dictionary.add(line.strip())
        text_dictionary = list(fp_dictionary)
        return text_dictionary

    @staticmethod
    def generate_taxonomy_dict(dict_file_path):
        taxonomy_dict = {}
        with open(dict_file_path, 'r') as f:
            for line in f.readlines():
                key, value = line.split(':')
                taxonomy_dict[key.strip()] = value.strip().split(',')
        return taxonomy_dict

    def run_single_session(self, image, out_img_savedir):
        '''
        :param image: cv2.imread() image object (BGR)
        :return:
        file_name: str - file path of image saved with bbox drawn
        objects_list - json - json object of output data from OD model
        '''
        print('------Object Detection Begins------')
        start_time = time.time()
        image_np = self.load_image_into_numpy_array(image)
        image_w, image_h, _ = image_np.shape
        image_ready_time = time.time()
        print("Image ready for model takes", image_ready_time-start_time)
        output_dict = self.__sess.run(self.__tensor_dict, feed_dict={self.__image_tensor: np.expand_dims(image_np, 0)})
        session_run_time = time.time()
        print("Session running  takes", session_run_time-image_ready_time)
        output_dict['num_detections'] = int(output_dict['num_detections'][0])
        output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint16)
        output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
        output_dict['detection_scores'] = output_dict['detection_scores'][0]

        # Increase the bbox height and width by 10% in each dirction
        output_dict['detection_boxes'] = self.increase_bbox_area(output_dict['detection_boxes'], image_h, image_w,
                                                            self.__bbox_enlarge_ratio)
        output_image = vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            self.__category_index,
            max_boxes_to_draw=None,
            min_score_thresh=self.__min_confidence_text_detection,
            instance_masks=output_dict.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=3)
        image_visualization_time = time.time()
        print("Image Bounding Box visualization takes", image_visualization_time-session_run_time)
        objects_list = vis_util.output_to_json(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            self.__category_index,
            min_score_thresh=self.__min_confidence_text_detection)
        json_generating_time = time.time()
        print("JSON generation takes", json_generating_time-image_visualization_time)

        img = Image.fromarray(output_image)
        file_name = out_img_savedir + '.jpg'
        img.save(file_name)
        image_saving_time = time.time()
        print("Image saving takes", image_saving_time-json_generating_time)
        return file_name, objects_list

    @staticmethod
    def load_image_into_numpy_array(image):
        """Return a 3-D image numpy array and shape

        Args:
            image_path: path to the image

        Returns:
            3D image numpy array od shape (height, width, 3) dtype: uint8
        """
        # img = cv2.imread(image_path)
        image_np = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return image_np

    @staticmethod
    def increase_bbox_area(bbox_list, image_h, image_w, bbox_enlarge_ratio):
        '''

        :param bbox_list: list - list of bbox cordinates list in an image
        :param image_h: int - height of original image
        :param image_w: int - width of original image
        :param bbox_enlarge_ratio: float - ratio by which bbox must be enlarged in all four direction
        :return:
        np.array of new bbox cordinates list in an image
        '''
        larger_bbox_list = []
        for bbox in bbox_list:
            xmin, ymin, xmax, ymax = bbox
            h_ratio = (ymax - ymin) * bbox_enlarge_ratio
            w_ratio = (xmax - xmin) * bbox_enlarge_ratio
            new_xmin = max((xmin - w_ratio), 0)
            new_ymin = max((ymin - h_ratio), 0)
            new_xmax = min((xmax + w_ratio), image_w)
            new_ymax = min((ymax + h_ratio), image_h)
            larger_bbox_list.append([new_xmin, new_ymin, new_xmax, new_ymax])
        return np.array(larger_bbox_list)

    @staticmethod
    def print_text_from_image(im):
        '''
        Return text from the image using tesseract
        :param im: cv2.imread() object
        :return str
        '''
        # Uncomment the line below to provide path to tesseract manually
        # pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract'

        # Define config parameters.
        # '-l eng'  for using the English language
        # '--oem 1' for using LSTM OCR Engine
        start_time = time.time()
        config = ('--tessdata-dir /home/dsserver/.tessdata/tessdata -l eng --oem 1 --psm 6') # 6

        # Run tesseract OCR on image
        text = pytesseract.image_to_string(im, config=config)
        text = text.replace('\n', ' ')
        text = text.encode('ascii', 'ignore').decode("utf-8")
        text = text.upper()
        print('Tesseract ocr time taken: ', time.time()-start_time)

        return text

    @staticmethod
    def get_correct_text(text, text_dictionary):
        start_time = time.time()
        # correct text format
        # replace non-alphabets with '' or ' '
        # ignore single char str
        text = str(text)
        text_formatted = ''
        for charx in text:
            if charx.isalpha() or charx == ' ' :
                text_formatted += charx

        text = text_formatted
        text_list = str(text).split(' ')
        text_match = ''
        unmatched_word = []
        match_score = 0
        counter = 0
        for word in text_list:
            word = str(word).strip()
            if len(word) <= 1:
                continue
            word_match, word_match_score = process.extractOne(word, text_dictionary, scorer=fuzz.token_sort_ratio)

            if word_match_score > 80:
                text_match = text_match + ' ' + word_match
                match_score += word_match_score
                counter += 1
            else:
                text_match = text_match + '' #' ' + '???'
                unmatched_word.append(word)

        with open(SessionManagerOCRFP.ocr_unmatch_word_log_path, 'a') as f:
            for word in unmatched_word:
                f.write(word+'/n')

        text_match = text_match.strip()
        try:
            match_score /= counter
        except ZeroDivisionError:
            match_score = 0
        print('Text correction time taken: ', time.time() - start_time)
        return text_match, match_score

    @staticmethod
    def taxonomy_mapping(text_match_list, taxonomy_dict):
        category_list = []
        for text in text_match_list:
            text = str(text).strip()
            if len(text) > 100:
                continue
            found = False
            for key in taxonomy_dict:
                for cat in taxonomy_dict[key]:
                    if cat in text:
                        category_list.append(key)
                        found = True
                        break
                if found:
                    break

        #TODO : Bed, bath count
        bed_count = 0 #category_list.count('Bedroom')
        bath_count = 0 #category_list.count('Bathroom')
        category_list = list(set(category_list))
        return bed_count, bath_count, category_list

    @staticmethod
    def ocr_on_bbox(bbox):
        print('OCR on bbox ', bbox['box'])
        crop_h, crop_w = bbox['image'].shape
        aspect_ratio = crop_w / crop_h
        # ocr tesseract
        try:
            text = SessionManagerOCRFP.print_text_from_image(bbox['image'])
            text_match, match_score = SessionManagerOCRFP.get_correct_text(text, SessionManagerOCRFP.text_dictionary)
        except Exception as e:
            text_match = text = '##FAILED TESSERACT##'
            match_score = 0

        bbox['text'] = str(text)
        bbox['text_match'] = str(text_match)
        bbox['match_score'] = str(match_score)

        # Rotate the vertical oriented text using aspect ratio
        if aspect_ratio < 0.75:
            print('Vertical oriented text')
            rot1 = cv2.rotate(bbox['image'], rotateCode=cv2.ROTATE_90_CLOCKWISE)
            rot2 = cv2.rotate(bbox['image'], rotateCode=cv2.ROTATE_90_COUNTERCLOCKWISE)
            text1 = SessionManagerOCRFP.print_text_from_image(rot1)
            text2 = SessionManagerOCRFP.print_text_from_image(rot2)
            text_match1, match_score1 = SessionManagerOCRFP.get_correct_text(text1, SessionManagerOCRFP.text_dictionary)
            text_match2, match_score2 = SessionManagerOCRFP.get_correct_text(text2, SessionManagerOCRFP.text_dictionary)
            bbox['rotation1'] = str(text_match1)
            bbox['rotation2'] = str(text_match2)

        bbox.pop('image')
        return bbox

    @staticmethod
    def detect_floor_level(text_list, category_list):
        floor_level = set()

        expected_first_floor_cats = ['Foyer', 'Front Porch', 'Garage']
        for cat in category_list:
            if cat in expected_first_floor_cats:
                print('First floor category detected: ', cat)
                floor_level.add(1)
                break

        for key, value in SessionManagerOCRFP.floor_level_dict.items():
            for word in value:
                for text in text_list:
                    is_same = True
                    for sub_str in word.split(' '):
                        if sub_str not in text:
                            is_same = is_same and False
                    if is_same:
                        floor_level.add(int(key))
                        break

                    # if fuzz.token_sort_ratio(text, word) == 100:
                    #     floor_level.add(int(key))
                    #     break

        return list(floor_level)

    def get_image_text_ocr(self, image):
        '''
        :param image: cv2.imread() image object (BGR)
        :return:
        file_name: str , path to image with bbox generated
        json_return: json, image text output data
        '''
        start_time = time.time()
        img_name = str(int(round(time.time() * 1000)))
        out_img_savedir = os.path.join(self.image_output_path, img_name)
        if not os.path.exists(out_img_savedir):
            os.makedirs(out_img_savedir)

        # Generate text bbox in image
        file_name, objects_list = self.run_single_session(image, out_img_savedir)
        box_detection_time = time.time()

        # Convert image to gray and crop bbox for ocr
        orig_gray = image.copy()
        orig_gray = cv2.cvtColor(orig_gray, cv2.COLOR_BGR2GRAY)
        file_str = ''
        ocr_objects_list = []
        box_counter = [i for i in range(objects_list.__len__())]

        for i in box_counter:
            bbox = objects_list[i]
            startX, startY, endX, endY = int(bbox["xmin"]), int(bbox["ymin"]), int(bbox["xmax"]), int(bbox["ymax"])

            bbox.pop("ObjectLabel")
            bbox.pop("Prob")
            bbox.pop("xmin")
            bbox.pop("ymin")
            bbox.pop("xmax")
            bbox.pop("ymax")
            bbox['cordinates'] = (startX, startY, endX, endY)
            # crop the box
            cropped = orig_gray[startY:endY, startX:endX]
            # Increase image resolution
            crop_h, crop_w = cropped.shape
            if crop_h * crop_w < 12000:
                try:
                    cropped = cv2.resize(cropped, None, fx=3, fy=3, interpolation=cv2.INTER_CUBIC)
                except Exception as e:
                    print("caught exception in crop ", cropped)

            bbox['box'] = i
            bbox['image'] = cropped
            # save the bboxes cropped
            img_savepath = os.path.join(out_img_savedir, (str(box_counter[i]) + '_bbox.jpg'))
            cv2.imwrite(img_savepath, cropped)

        ocr_prep_time = time.time()
        os.environ['OMP_THREAD_LIMIT'] = '1'
        with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
            for bbox in executor.map(SessionManagerOCRFP.ocr_on_bbox, objects_list):
                ocr_objects_list.append(bbox)

        ocr_completion_time = time.time()
        text_list = [str(obj['text']).strip() for obj in ocr_objects_list]
        text_match_list = [str(obj['text_match']).strip() for obj in ocr_objects_list]

        # Generate json to return output
        json_return = {}
        json_return['ocr'] = ocr_objects_list
        summary_dict = {}
        bed_count, bath_count, category_list = SessionManagerOCRFP.taxonomy_mapping(text_match_list,
                                                                                    SessionManagerOCRFP.taxonomy_dict)
        summary_dict['bed'] = bed_count
        summary_dict['bath'] = bath_count
        summary_dict['categories'] = category_list

        # detect floor level floor level
        summary_dict['floor-level'] = SessionManagerOCRFP.detect_floor_level(text_list, category_list)

        json_return['summary'] = summary_dict

        #Save result output to json file
        json_data = json.dumps(json_return)
        img_savepath = os.path.join(out_img_savedir, img_name)
        json_savepath = img_savepath + '.json'
        with open(json_savepath, 'w+') as f:
            f.write(json_data)

        print('Total time for box detection: ', box_detection_time - start_time)
        print('Time for ocr preparation: ', ocr_prep_time - box_detection_time)
        print('Total time for ocr completion for all bbox: ', ocr_completion_time - ocr_prep_time)
        print('Total time for execution: ', time.time() - start_time)
        return file_name, json_return


'''
Requires files:
od_utils
model dir
tesseract language dictionary
'''
