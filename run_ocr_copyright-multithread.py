import cv2
import sys
import os, time
import pandas as pd
import shutil
#from multiprocessing.pool import ThreadPool
import concurrent.futures
from ocr_copyright_session_manager_multithread import SessionManagerOCRFP
from common_utils import ImageUtils


def process_image_url(idx_url):
    """
    Process a given image url to compute it's metadata and resize it for prediction

    Arguments:
    :param idx_url: id, url tuple
    :param image_params: all parameters related to image processing

    :return: a dictionary of image properties including name, url, resized image, metadata
    :type image_url: str
    :type image_params: dict
    """
    #status = 'OK'
    image_url = idx_url[1]
    img_download_path='./resources/copyright-ocr'
    image_name = image_url.rsplit('/', 1)[-1]
    image_name = str(idx_url[0]) + '.' + image_name.rsplit('.',1)[-1]
    full_image_path = '/'.join([img_download_path, image_name])
    download_status, dl_status = ImageUtils.get_image_from_url(image_url, full_image_path)
    file_read_status = False
    original_image = None
    try:
        if download_status != 2:  # try to read only downloaded images
                original_image = ImageUtils.read_image(full_image_path)
                if original_image is not None:
                    file_read_status = True
                else:
                    status = 'Read Error'
                    print('ImageClassifierError - Read Error,{},{}'.format(image_url, full_image_path))
        else:
            status = 'Download Error'
            logger.error('ImageClassifierError - Download Error {},{},{}'.format(dl_status,image_url, full_image_path))
    except Exception as ex:
        print('ImageClassifierError - Download/Read Error,{},{}'.format(image_url, full_image_path))
    
    return (idx_url[0], idx_url[1], original_image, file_read_status)


def main():
    batch = 18
    process_name = 'run_ocr_copyright-multithread-{}'.format(str(batch))
    process_control_log_file = 'batch-process-control-log.txt'
    with open(process_control_log_file,'r') as log_file:
        for line in log_file:
            line = line.strip()
            if line==process_name:
                print("Completed already, ending process ", process_name)
                sys.exit(0)

    print("Begin process ", process_name)
    # GPU consumption ~ 3.3gb
    # Initialise the OD model
    start_time = time.time()
    tf_sess_ocr = SessionManagerOCRFP("./ocr_fp_graph")
    model_load_time = time.time()

      
    img_csv_path = './resources/process_result_out/batch-{}/{}-copyright.csv'.format(str(batch), str(batch)) # change path
    json_save_dir = os.path.join(os.path.dirname(img_csv_path),'json_out_copyright')
    img_download_path='./resources/copyright-ocr-{}/'.format(batch)
    
    if not os.path.exists(img_csv_path):
        print("img_csv_path does not exist, ending process.")
        sys.exit(0)

    if not os.path.exists(json_save_dir):
        os.makedirs(json_save_dir)

    if not os.path.exists(img_download_path):
        os.makedirs(img_download_path)

    df_img = pd.read_csv(img_csv_path)
    n_records = df_img.shape[0]
    counter = 0
    if 'ocr-status' not in df_img.columns:
        df_img['download-status'] = [False] * n_records
        df_img['ocr-status'] = [False] * n_records
        df_img['json_path'] = [ os.path.join(json_save_dir, str(i)+'.json') for i in range(n_records) ]
        df_img.to_csv(img_csv_path)


    batch_size = 100
    idx_url_list = []
    for idx, row in df_img.iterrows():
        if row['ocr-status']:
            print(idx, ' row- done already')
            continue
        print(idx, ' row- to download and ocr')
        idx_url_list.append((idx,row['Url']))
        
        if (len(idx_url_list) == batch_size) or (idx==n_records-1):
            print('Start downloading the batch...')
            download_start = time.time()
            #idx_url_image_list = ThreadPool(10).imap_unordered(process_image_url, idx_url_list)
            with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
                idx_url_image_list = executor.map(process_image_url, idx_url_list)
            print('Finish downloading the batch... Time: ', time.time()-download_start)
            idx_url_list = []
            for record in idx_url_image_list:
                counter += 1
                df_img.at[record[0],'download-status'] = record[3]
                if record[3]:
                    try:
                        # do ocr
                        _ = tf_sess_ocr.get_image_text_ocr(record[2], df_img.at[record[0],'json_path'])
                        df_img.at[record[0],'ocr-status'] = True
                        print('Successful ocr ', record[0])
                    except Exception as e:
                        print('Failed OCR. Caught exception at id ', record[0])
                        df_img.at[record[0],'ocr-status'] = False
            
            if 'ocr-status' in df_img.columns: # to avoid saving null dataframes
                df_img.to_csv(img_csv_path)
            print('Saved batch !!')
            # CLEAN DOWNLOAD DIRECTORY 
            shutil.rmtree(img_download_path)
            if not os.path.exists(img_download_path):
                os.makedirs(img_download_path)
    

    all_ocr_time = time.time()

    with open(process_control_log_file, 'a') as log_file:
        log_file.write(process_name + '\n')

    print("Model load time: {} secs".format(model_load_time - start_time))
    print("Time for {} image ocr {} secs".format(counter, all_ocr_time - model_load_time))


if __name__ == "__main__":
    main()
