import base64
import mimetypes
import socket
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

import httplib2
from googleapiclient.discovery import build
from oauth2client import client
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import ClientRedirectServer, ClientRedirectHandler
from oauth2client.tools import _FAILED_START_MESSAGE, _GO_TO_LINK_MESSAGE, _BROWSER_OPENED_MESSAGE

from logs_utils import get_default_logger


def run_custom_flow(flow, storage, http):
    logger = get_default_logger()
    auth_host_name ='localhost'
    auth_host_port = [8080, 8090, 8100]
    noauth_local_webserver = False
    if not noauth_local_webserver:
        success = False
        port_number = 0
        for port in auth_host_port:
            port_number = port
            try:
                httpd = ClientRedirectServer((auth_host_name, port),
                                             ClientRedirectHandler)
            except socket.error:
                pass
            else:
                success = True
                break
        noauth_local_webserver = not success
        if not success:
            logger.info(_FAILED_START_MESSAGE)

    if not noauth_local_webserver:
        oauth_callback = 'http://{host}:{port}/'.format(
            host=auth_host_name, port=port_number)
    else:
        oauth_callback = client.OOB_CALLBACK_URN
    flow.redirect_uri = oauth_callback
    authorize_url = flow.step1_get_authorize_url()

    if not noauth_local_webserver:
        import webbrowser
        webbrowser.open(authorize_url, new=1, autoraise=True)
        logger.debug(_BROWSER_OPENED_MESSAGE.format(address=authorize_url))
    else:
        logger.debug(_GO_TO_LINK_MESSAGE.format(address=authorize_url))

    code = None
    if not noauth_local_webserver:
        httpd.handle_request()
        if 'error' in httpd.query_params:
            logger.error('Authentication request was rejected.')
        if 'code' in httpd.query_params:
            code = httpd.query_params['code']
        else:
            logger.info('Failed to find "code" in the query parameters '
                        'of the redirect.')
            logger.info('Try running with --noauth_local_webserver.')
    else:
        code = raw_input('Enter verification code: ').strip()

    try:
        credential = flow.step2_exchange(code, http=http)
    except client.FlowExchangeError as e:
        logger.error('Authentication has failed: {0}'.format(e))

    storage.put(credential)
    credential.set_store(storage)
    logger.info('Authentication successful.')

    return credential


def build_service(secret_file, credentials_path, oauth_scope='https://www.googleapis.com/auth/gmail.compose'):
    """
    Create an email service to send (default) or receive messages

    Arguments:
    :param secret_file:
    :param oauth_scope: Type of email action (default: send)
    :param credentials_path: where to store messages (default: gmail storage)

    :return:
    :type secret_file: str
    :type oauth_scope: str
    :type credentials_path: str
    """

    authorize = httplib2.Http()
    credentials = None
    if credentials_path is not None:
        credentials_storage = Storage(credentials_path)
        credentials = credentials_storage.get()
    if credentials is None or credentials.invalid:
        flow = flow_from_clientsecrets(secret_file, scope=oauth_scope)
        credentials = run_custom_flow(flow, credentials_storage, http=authorize)
    authorize = credentials.authorize(authorize)
    return build('gmail', 'v1', http=authorize)


def create_message(sender, receiver, subject, text):
    """
    Create an email message

    Arguments:
    :param sender: valid email id
    :param receiver: email id
    :param subject: message subject
    :param text: message body

    :return: returns an instance of MIME Text message
    :type sender: str
    :type receiver: str
    :type subject: str
    :type text: str
    """

    message = MIMEText(text)
    message['from'] = sender
    message['to'] = receiver
    message['subject'] = subject
    return {'raw': base64.b64encode(message.as_string())}


def read_attachment(file_path):
    content_type, encoding = mimetypes.guess_type(file_path)
    if content_type is None or encoding is not None:
        content_type = 'application/octet-stream'

    main_type, sub_type = content_type.split('/', 1)

    def get_type(file_type):
        with open(file_path, 'rb') as fp:
            return file_type(fp.read(), _subtype=sub_type)

    if main_type == 'text':
        attachment = get_type(MIMEText)
    elif main_type == 'image':
        attachment = get_type(MIMEImage)
    elif main_type == 'audio':
        attachment = get_type(MIMEAudio)
    else:
        attachment = MIMEBase(main_type, sub_type)
        with open(file_path, 'rb') as f:
            attachment.set_payload(f.read())

    file_name = basename(file_path)
    attachment.add_header('Content-Disposition', 'attachment', filename=file_name)
    return attachment


def create_message_with_attachment(sender, receiver, subject, text, file_path):
    message = MIMEMultipart()
    message['from'] = sender
    message['to'] = receiver
    message['subject'] = subject
    message.attach(MIMEText(text))

    attachment = read_attachment(file_path)
    message.attach(attachment)

    return {'raw': base64.urlsafe_b64encode(message.as_string())}


def send_mail(sender, receivers, subject, text, secret_file, credentials_path=None, attachment=None):
    """
    Send mail to one or more receivers using Gmail

    Arguments:
    :param sender: valid email id
    :param receivers: list of valid email ids
    :param subject: message subject
    :param text: message body
    :param secret_file: full path to client secret file

    Keyword arguments:
    :param credentials_path: Path to gmail credentials file
    :param attachment: Attachment to be sent with mail

    :return: return status of sent mail
    :type sender:str
    :type receivers:list
    :type subject:str
    :type text:str
    :type secret_file:str
    """

    receiver = ', '.join(receivers)
    if attachment is not None:
        message = create_message_with_attachment(sender, receiver, subject, text, attachment)
    else:
        message = create_message(sender, receiver, subject, text)
    mail_service = build_service(secret_file, credentials_path)
    logger = get_default_logger()
    try:
        mail_service.users().messages().send(userId="me", body=message).execute()
        logger.info('Sent email to : {}'.format(receiver))
        return True
    except Exception as error:
        logger.error('Error!! sending mail: {}'.format(error))
        return False

