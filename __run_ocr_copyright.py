import cv2
import os, time
import pandas as pd
from ocr_copyright_session_manager_multithread import SessionManagerOCRFP
from common_utils import ImageUtils


# GPU consumption ~ 3.3gb
# Initialise the OD model
start_time = time.time()
tf_sess_ocr = SessionManagerOCRFP("./ocr_fp_graph")
model_load_time = time.time()

def process_image_url(image_url, img_download_path):
    """
    Process a given image url to compute it's metadata and resize it for prediction

    Arguments:
    :param image_url: full image url
    :param image_params: all parameters related to image processing

    :return: a dictionary of image properties including name, url, resized image, metadata
    :type image_url: str
    :type image_params: dict
    """
    #status = 'OK'
    image_name = image_url.rsplit('/', 1)[-1]
    full_image_path = '/'.join([img_download_path, image_name])
    download_status, dl_status = ImageUtils.get_image_from_url(image_url, full_image_path)
    file_read_status = False
    original_image = None
    try:
        if download_status != 2:  # try to read only downloaded images
                original_image = ImageUtils.read_image(full_image_path)
                if original_image is not None:
                    file_read_status = True
                else:
                    status = 'Read Error'
                    print('ImageClassifierError - Read Error,{},{}'.format(image_url, full_image_path))
        else:
            status = 'Download Error'
            logger.error('ImageClassifierError - Download Error {},{},{}'.format(dl_status,image_url, full_image_path))
    except Exception as ex:
        print('ImageClassifierError - Download/Read Error,{},{}'.format(image_url, full_image_path))
    
    return original_image

def batch_ocr_process(imgurl, json_savepath):
	try:
		image = process_image_url(image_url=imgurl, img_download_path='/tmp/copyright-ocr/') # get downloaded cv2 image
		_ = tf_sess_ocr.get_image_text_ocr(image, json_savepath)
		print("Saved OCR output at ", json_savepath)
		return True
	except Exception as e:
		print("$$$$$ Caught exception", e)
		return False


# #Running on image directory
# counter = 0
# imgdir = './static/upload/Copyright'
# json_save_dir = './resources/json_out_copyright'
# img_list = [name for name in os.listdir(imgdir) if name.endswith('.jpg') or name.endswith('.jpeg') or name.endswith('.png')]
# for imgname in img_list:
# 	counter += 1
# 	imgpath = os.path.join(imgdir, imgname)
# 	json_savepath = os.path.join(json_save_dir, str(counter) + '.json')
# 	image = cv2.imread(imgpath)
# 	_ = tf_sess_ocr.get_image_text_ocr(image, json_savepath)
# 	print("Saved OCR output at ", json_savepath)

# Running on csv list
img_download_path = '/tmp/copyright-ocr/'
#Change both csv and json
img_csv_path = './resources/process_result_out/batch-2/2-copyright.csv'
json_save_dir = './resources/process_result_out/batch-2/json_out_copyright'
df_img = pd.read_csv(img_csv_path)
if 'ocr-status' not in df_img.columns:
	df_img['ocr-status'] = [False] * df_img.shape[0]
	df_img['json_path'] = [ os.path.join(json_save_dir, str(i)+'.json') for i in range(df_img.shape[0]) ]
	df_img.to_csv(img_csv_path)

img_download_path='/tmp/copyright-ocr/'
if not os.path.exists(img_download_path):
    os.makedirs(img_download_path)
counter = 0
for idx, row in df_img.iterrows():
	print('Checking row ', idx)
	if row['ocr-status']:
		idx += 100
		continue
	else:
		df_img.at[idx,'ocr-status'] = batch_ocr_process(row['Url'], row['json_path'])
		counter += 1

	if (idx%100) == 0:
		df_img.to_csv(img_csv_path)

#Save ipdated csv
df_img.to_csv(img_csv_path)

all_ocr_time = time.time()

print("Model load time: {} secs".format(model_load_time - start_time))
print("Time for {} image ocr {} secs".format(counter, all_ocr_time - model_load_time))