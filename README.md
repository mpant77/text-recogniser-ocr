# README #

This code is meant for text recognition from image - ocr.
It uses google tesseract 4 and a trained text detector in the form of tensorflow fronzen graph.
Text detector is nothing but a faster rcnn object detector which detects and locates text in the images and 
outputs the cordinates of text box. These cordinates are cropped and pre-processed for text recognition.