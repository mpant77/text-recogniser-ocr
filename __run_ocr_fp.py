import cv2
import os, time
from ocr_fp_session_manager_multithread import SessionManagerOCRFP


# GPU consumption ~ 3.3gb
# Initialise the OD model
start_time = time.time()
tf_sess_ocr = SessionManagerOCRFP("./ocr_fp_graph")
model_load_time = time.time()

counter = 0
imgdir = './static/upload/Copyright'
img_list = [name for name in os.listdir(imgdir) if name.endswith('.jpg') or name.endswith('.jpeg') or name.endswith('.png')]
for imgname in img_list:
	counter += 1
	imgpath = os.path.join(imgdir, imgname)
	image = cv2.imread(imgpath)
	file_name, ocr_objects_list = tf_sess_ocr.get_image_text_ocr(image)
	print("Saved OCR output at ", file_name)

all_ocr_time = time.time()

print("Model load time: {} secs".format(model_load_time - start_time))
print("Time for {} image ocr {} secs".format(counter, all_ocr_time - model_load_time))