import cv2
import numpy as np
import os
import pytesseract
import time

from PIL import Image
from tf_od_utils import label_map_util, seo_utils
from tf_od_utils import visualization_utils as vis_util

# if tf.__version__ < '1.4.0':
#     raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')


class SessionManagerOCR:
    """ TFSessionManager class handles loading a pretrained model as a servable
      and storing the session as a static class variable which can be run whenever 
      a request hits the endpoint.

      Arguments:
        model_dir: The path to directory where the frozen_inference_graph.pb and the labels file is located.
      """

    #__sess = None
    #__graph = None
    #__image_tensor = None
    #__tensor_dict = None
    #__category_index=None
    #model_dir = None

    def __init__(self, model_dir):
        self.model_dir = model_dir
        self.__layerNames = None
        self.__net = None
        self.__resize_width = 320
        self.__resize_height = 320
        self.__min_confidnce_text_detection = 0.7
        self.initialize()

    #@staticmethod
    def initialize(self):
        east_detector_graph_file = os.path.join(self.model_dir, 'frozen_east_text_detection.pb')

        # define the two output layer names for the EAST detector model that
        # we are interested -- the first is the output probabilities and the
        # second can be used to derive the bounding box coordinates of text
        layerNames = [
            "feature_fusion/Conv_7/Sigmoid",
            "feature_fusion/concat_3"]

        # load the pre-trained EAST text detector
        print("[INFO] loading EAST text detector...")
        net = cv2.dnn.readNet(east_detector_graph_file)
        self.__layerNames = layerNames
        self.__net = net


    def print_text_from_image(self, im):
        '''
        Return text from the image using tesseract
        :param im: cv2.imread() object
        :return str
        '''
        # Uncomment the line below to provide path to tesseract manually
        # pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract'

        # Define config parameters.
        # '-l eng'  for using the English language
        # '--oem 1' for using LSTM OCR Engine
        config = ('--tessdata-dir /home/dsserver/.tessdata/tessdata -l eng --oem 1 --psm 6')

        # Run tesseract OCR on image
        text = pytesseract.image_to_string(im, config=config)
        text = text.replace('\n', ' ')
        text = text.encode('ascii', 'ignore').decode("utf-8")

        return text


    def merge_boxes(self, box1, box2):
        '''
        Merge two boxes and returns new box cordinates.
        :param
        box1 = list of upper-left(x1, y1) and bottom-right(x2, y2) cordinates [x1, y1, x2, y2]
        box2 = list of upper-left(x1, y1) and bottom-right(x2, y2) cordinates [x1, y1, x2, y2]

        :return
        box3 = list of upper-left(x1, y1) and bottom-right(x2, y2) cordinates [x1, y1, x2, y2]
        '''
        x1 = min(box1[0], box2[0])
        y1 = min(box1[1], box2[1])

        x2 = max(box1[2], box2[2])
        y2 = max(box1[3], box2[3])

        box3 = [x1, y1, x2, y2]
        return box3

    def get_iou(self, bb1, bb2):
        """
        Calculate the Intersection over Union (IoU) of two bounding boxes.

        :param
        ----------
        bb1 : list
            ['x1', 'x2', 'y1', 'y2']
            The (x1, y1) position is at the top left corner,
            the (x2, y2) position is at the bottom right corner
        bb2 : list
            ['x1', 'x2', 'y1', 'y2']
            The (x, y) position is at the top left corner,
            the (x2, y2) position is at the bottom right corner

        :return
        -------
        float
            in [0, 1]
        """
        assert bb1[0] < bb1[2]
        assert bb1[1] < bb1[3]
        assert bb2[0] < bb2[2]
        assert bb2[1] < bb2[3]

        # determine the coordinates of the intersection rectangle
        x_left = max(bb1[0], bb2[0])
        y_top = max(bb1[1], bb2[1])
        x_right = min(bb1[2], bb2[2])
        y_bottom = min(bb1[3], bb2[3])

        if x_right < x_left or y_bottom < y_top:
            return 0.0

        # The intersection of two axis-aligned bounding boxes is always an
        # axis-aligned bounding box
        intersection_area = (x_right - x_left) * (y_bottom - y_top)

        # compute the area of both AABBs
        bb1_area = (bb1[2] - bb1[0]) * (bb1[3] - bb1[1])
        bb2_area = (bb2[2] - bb2[0]) * (bb2[3] - bb2[1])

        # compute the intersection over union by taking the intersection
        # area and dividing it by the sum of prediction + ground-truth
        # areas - the interesection area
        iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
        assert iou >= 0.0
        assert iou <= 1.0
        return iou

    def merge_overlapping_boxes(self, arects, thres_iou=0.05):
        '''
        Merges all the overlapping boxes from given list of boxes
        :param
        arects: list of tuples (x1, y1, x2, y2)
        :param
        thres_iou: float
        in [0, 1]
        :return: list of list [x1, y1, x2, y2]
        '''
        flag = 0
        i = 0

        while (i < len(arects)):
            pick_box = arects[i]
            for j in range(i + 1, len(arects)):
                if self.get_iou(pick_box, arects[j]) >= thres_iou:
                    pick_box = self.merge_boxes(pick_box, arects[j])
                    flag = 1
                    break

            if flag == 1:  # Overlapping was found
                tmp = [pick_box]
                tmp.extend(arects[:i])
                tmp.extend(arects[i + 1:j])
                tmp.extend(arects[j + 1:len(arects)])
                arects = tmp
                flag = 0
                i = 0

            else:
                i += 1
        return arects

    def east_text_detection(self, image):
        (H, W) = image.shape[:2]
        # construct a blob from the image and then perform a forward pass of
        # the model to obtain the two output layer sets
        blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),
                                     (123.68, 116.78, 103.94), swapRB=True, crop=False)
        self.__net.setInput(blob)
        start = time.time()
        # predict text bboxes
        (scores, geometry) = self.__net.forward(self.__layerNames)
        end = time.time()
        # show timing information on text prediction
        print("[INFO] east text detection took {:.6f} seconds".format(end - start))
        return (scores, geometry)


    def get_image_text_ocr(self, image):
        '''
        :param image: cv2 object bgr
        :param features:
        :return:
        '''
        start = time.time()
        objects_list = []
        orig = image.copy()
        (H, W) = image.shape[:2]

        # set the new width and height and then determine the ratio in change
        # for both the width and height
        rW = W / float(self.__resize_width)
        rH = H / float(self.__resize_height)

        # resize the image and grab the new image dimensions
        image = cv2.resize(image, (self.__resize_width, self.__resize_height))

        # predict text bboxes
        (scores, geometry) = self.east_text_detection(image)

        # grab the number of rows and columns from the scores volume, then
        # initialize our set of bounding box rectangles and corresponding
        # confidence scores
        (numRows, numCols) = scores.shape[2:4]
        rects = []
        confidences = []

        # loop over the number of rows
        for y in range(0, numRows):
            # extract the scores (probabilities), followed by the geometrical
            # data used to derive potential bounding box coordinates that
            # surround text
            scoresData = scores[0, 0, y]
            xData0 = geometry[0, 0, y]
            xData1 = geometry[0, 1, y]
            xData2 = geometry[0, 2, y]
            xData3 = geometry[0, 3, y]
            anglesData = geometry[0, 4, y]

            # loop over the number of columns
            for x in range(0, numCols):
                # if our score does not have sufficient probability, ignore it
                if scoresData[x] < self.__min_confidnce_text_detection:
                    continue

                # compute the offset factor as our resulting feature maps will
                # be 4x smaller than the input image
                (offsetX, offsetY) = (x * 4.0, y * 4.0)

                # extract the rotation angle for the prediction and then
                # compute the sin and cosine
                angle = anglesData[x]
                cos = np.cos(angle)
                sin = np.sin(angle)

                # use the geometry volume to derive the width and height of
                # the bounding box
                h = xData0[x] + xData2[x]
                w = xData1[x] + xData3[x]

                # compute both the starting and ending (x, y)-coordinates for
                # the text prediction bounding box
                endX = min(int(offsetX + (cos * xData1[x]) + (sin * xData2[x])), W)
                endY = min(int(offsetY - (sin * xData1[x]) + (cos * xData2[x])), H)
                startX = max(int(endX - w), 0)
                startY = max(int(endY - h), 0)

                # add the bounding box coordinates and probability score to
                # our respective lists
                rects.append((startX, startY, endX, endY))
                confidences.append(scoresData[x])


        # Merge all the overlapping bboxes
        arects = self.merge_overlapping_boxes(rects)

        counter = 0
        file_str = ''
        orig_gray = orig.copy()
        orig_gray = cv2.cvtColor(orig_gray, cv2.COLOR_BGR2GRAY)

        img_parent_dir = './static/images/Output_OCR'
        img_name = str(int(round(time.time() * 1000))) + '.jpg'
        out_img_savedir = os.path.join(img_parent_dir, img_name)
        if not os.path.exists(out_img_savedir):
            os.makedirs(out_img_savedir)

        # loop over the bboxes and generate text ocr
        for (startX, startY, endX, endY) in arects:
            box_dict = {}
            counter += 1
            # scale the bounding box coordinates based on the respective
            # ratios
            startX = int(startX * rW)
            startY = int(startY * rH)
            endX = int(endX * rW)
            endY = int(endY * rH)

            # TODO: make sure correct image format and cordinates reach here
            # crop the box
            cropped = orig_gray[startY:endY, startX:endX]
            # Increase image resolution
            try:
                cropped = cv2.resize(cropped, None, fx=3, fy=3, interpolation=cv2.INTER_CUBIC)
            except Exception as e:
                print("caught exception in crop ", cropped)

            # save the bboxes cropped
            img_savepath = os.path.join(out_img_savedir, (str(counter) + '_bbox.jpg'))
            cv2.imwrite(img_savepath, cropped)

            # ocr tesseract
            try:
                text = self.print_text_from_image(cropped)
            except Exception as e:
                text = '##FAILED TESSERACT##'

            box_str = '\n-------bbox {}------\n'.format(counter) + \
                      'cordinates: [{}, {}, {}, {}]\n'.format(startX, startY, endX, endY) + \
                      text + '\n_____end_____\n'
            file_str = file_str + box_str
            print(box_str)

            box_dict['box'] = counter
            box_dict['cordinates'] = (startX, startY, endX, endY)
            box_dict['text'] = str(text)
            objects_list.append(box_dict)

            # draw the bounding box on the image
            cv2.rectangle(orig, (startX, startY), (endX, endY), (0, 255, 0), 2)

        end = time.time()
        print("[INFO] text ocr took total time {:.6f} seconds".format(end - start))
        img_savepath = os.path.join(out_img_savedir, img_name)
        text_savepath = img_savepath + '.txt'
        with open(text_savepath, 'w+') as f:
            f.write(file_str)

        cv2.imwrite(img_savepath, orig)

        return img_savepath, objects_list