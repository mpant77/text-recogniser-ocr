import cv2
import numpy as np
import os
import tensorflow as tf
import time
import pytesseract

from fuzzywuzzy import fuzz
from fuzzywuzzy import process

from PIL import Image
from tf_od_utils import label_map_util, seo_utils
from tf_od_utils import visualization_utils as vis_util

# if tf.__version__ < '1.4.0':
#     raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')


class SessionManagerOCRFP:
    """ TFSessionManager class handles loading a pretrained model as a servable
      and storing the session as a static class variable which can be run whenever
      a request hits the endpoint.

      Arguments:
        model_dir: The path to directory where the frozen_inference_graph.pb and the labels file is located.
      """

    def __init__(self, model_dir):
        self.model_dir = model_dir
        self.__sess = None
        self.__graph = None
        self.__image_tensor = None
        self.__tensor_dict = None
        self.__category_index = None
        self.__min_confidence_text_detection = 0.5
        self.__bbox_enlarge_ratio = 0.1
        self.num_classes = 1
        self.initialize()

    #@staticmethod
    def initialize(self):
        PATH_TO_CKPT = os.path.join(self.model_dir, 'frozen_inference_graph.pb')
        PATH_TO_LABELS = os.path.join(self.model_dir, 'object-detection.pbtxt')
        NUM_CLASSES = self.num_classes
        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                                    use_display_name=True)
        self.__category_index = label_map_util.create_category_index(categories)
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
                # print "Model Successfully loaded from", TFSessionManager.model_dir
                config = tf.ConfigProto(allow_soft_placement=True)
                config.gpu_options.per_process_gpu_memory_fraction = 0.5
                sess = tf.Session(config=config)
                ops = detection_graph.get_operations()
                all_tensor_names = {output.name for op in ops for output in op.outputs}
                tensor_dict = {}
                for key in [
                    'num_detections', 'detection_boxes', 'detection_scores',
                    'detection_classes', 'detection_masks'
                ]:
                    tensor_name = key + ':0'
                    if tensor_name in all_tensor_names:
                        tensor_dict[key] = detection_graph.get_tensor_by_name(
                            tensor_name)
                self.__image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                self.__graph = detection_graph
                self.__sess = sess
                self.__tensor_dict = tensor_dict

    def increase_bbox_area(self, bbox_list, image_h, image_w, bbox_enlarge_ratio):
        larger_bbox_list = []
        for bbox in bbox_list:
            xmin, ymin, xmax, ymax = bbox
            h_ratio = (ymax - ymin) * bbox_enlarge_ratio
            w_ratio = (xmax - xmin) * bbox_enlarge_ratio
            new_xmin = max((xmin - w_ratio), 0)
            new_ymin = max((ymin - h_ratio), 0)
            new_xmax = min((xmax + w_ratio), image_w)
            new_ymax = min((ymax + h_ratio), image_h)
            larger_bbox_list.append([new_xmin, new_ymin, new_xmax, new_ymax])
        return np.array(larger_bbox_list)

    def run_single_session(self, image):
        print('------Object Detection Begins------')
        start_time = time.time()
        image_np = self.load_image_into_numpy_array(image)
        image_w, image_h, _ = image_np.shape
        image_ready_time = time.time()
        print("Image ready for model takes", image_ready_time-start_time)
        output_dict = self.__sess.run(self.__tensor_dict, feed_dict={self.__image_tensor: np.expand_dims(image_np, 0)})
        session_run_time = time.time()
        print("Session running  takes", session_run_time-image_ready_time)
        output_dict['num_detections'] = int(output_dict['num_detections'][0])
        output_dict['detection_classes'] = output_dict['detection_classes'][0].astype(np.uint16)
        output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
        output_dict['detection_scores'] = output_dict['detection_scores'][0]

        # Increase the bbox height and width by 10% in each dirction
        output_dict['detection_boxes'] = self.increase_bbox_area(output_dict['detection_boxes'], image_h, image_w,
                                                            self.__bbox_enlarge_ratio)
        output_image = vis_util.visualize_boxes_and_labels_on_image_array(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            self.__category_index,
            max_boxes_to_draw=None,
            min_score_thresh=self.__min_confidence_text_detection,
            instance_masks=output_dict.get('detection_masks'),
            use_normalized_coordinates=True,
            line_thickness=3)
        image_visualization_time = time.time()
        print("Image Bounding Box visualization takes", image_visualization_time-session_run_time)
        objects_list = vis_util.output_to_json(
            image_np,
            output_dict['detection_boxes'],
            output_dict['detection_classes'],
            output_dict['detection_scores'],
            self.__category_index,
            min_score_thresh=self.__min_confidence_text_detection)
        json_generating_time = time.time()
        print("JSON generation takes", json_generating_time-image_visualization_time)

        img = Image.fromarray(output_image)
        milliseconds = int(round(time.time() * 1000))
        file_name = './static/images/Output_OD_' + str(milliseconds) + '.jpg'
        img.save(file_name)
        image_saving_time = time.time()
        print("Image saving takes", image_saving_time-json_generating_time)
        return file_name, objects_list

    @staticmethod
    def load_image_into_numpy_array(image):
        """Return a 3-D image numpy array and shape

        Args:
            image_path: path to the image

        Returns:
            3D image numpy array od shape (height, width, 3) dtype: uint8
        """
        #img = cv2.imread(image_path)
        image_np = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return image_np

    def print_text_from_image(self, im):
        '''
        Return text from the image using tesseract
        :param im: cv2.imread() object
        :return str
        '''
        # Uncomment the line below to provide path to tesseract manually
        # pytesseract.pytesseract.tesseract_cmd = '/usr/bin/tesseract'

        # Define config parameters.
        # '-l eng'  for using the English language
        # '--oem 1' for using LSTM OCR Engine
        start_time = time.time()
        config = ('--tessdata-dir /home/dsserver/.tessdata/tessdata -l eng --oem 1 --psm 6') # 6

        # Run tesseract OCR on image
        text = pytesseract.image_to_string(im, config=config)
        text = text.replace('\n', ' ')
        text = text.encode('ascii', 'ignore').decode("utf-8")
        text = text.upper()
        print('Tesseract ocr time taken: ', time.time()-start_time)

        return text

    def get_correct_text(self, text, text_dictionary):
        start_time = time.time()
        #correct text format
        # replace non-alphabets with '' or ' '
        # ignore single chars
        text = str(text)
        text_formatted = ''
        for charx in text:
            if charx.isalpha() or charx == ' ' :
                text_formatted += charx

        text = text_formatted

        text_list = str(text).split(' ')
        text_match = ''
        unmatched_word = []
        match_score = 0
        counter = 0
        for word in text_list:
            word = str(word).strip()
            if len(word) <= 1:
                continue
            word_match, word_match_score = process.extractOne(word, text_dictionary, scorer=fuzz.token_sort_ratio)

            if word_match_score > 80:
                text_match = text_match + ' ' + word_match
                match_score += word_match_score
                counter += 1
            else:
                text_match = text_match + ' ' + '???'
                unmatched_word.append(word)

        with open('fp_ocr_data/log-unmatched-word.txt', 'a') as f:
            for word in unmatched_word:
                f.write(word+'/n')
        #text_match, match_score = process.extractOne(text, text_dictionary, scorer=fuzz.token_sort_ratio)
        text_match = text_match.strip()
        try:
            match_score /= counter
        except ZeroDivisionError:
            match_score = 0
        print('Text correction time taken: ', time.time() - start_time)
        return text_match, match_score

    def generate_taxonomy_dict(self, taxonomy_dict_file):
        start_time = time.time()
        taxonomy_dict = {}
        with open(taxonomy_dict_file, 'r') as f:
            for line in f.readlines():
                key, value = line.split(':')
                taxonomy_dict[key.strip()] = value.strip().split(',')
        print('Taxonomy dict generation time: ', time.time() - start_time)
        return taxonomy_dict

    def taxonomy_mapping(self, text_match_list, taxonomy_dict):
        start_time = time.time()
        category_list = []
        for text in text_match_list:
            text = str(text).strip()
            if len(text) > 100:
                continue
            found = False
            for key in taxonomy_dict:
                for cat in taxonomy_dict[key]:
                    if cat in text:
                        category_list.append(key)
                        found = True
                        break
                if found:
                    break

        bed_count = category_list.count('Bedroom')
        bath_count = category_list.count('Bathroom')
        category_list = list(set(category_list))
        print('Taxonomy mapping time: ', time.time() - start_time)
        return bed_count, bath_count, category_list

    def get_image_text_ocr(self, image):
        start_time = time.time()
        file_name, objects_list = self.run_single_session(image)
        box_detection_time = time.time()
        counter = 0
        file_str = ''
        orig_gray = image.copy()
        orig_gray = cv2.cvtColor(orig_gray, cv2.COLOR_BGR2GRAY)

        img_parent_dir = './static/images/Output_OCR_FP'
        img_name = str(int(round(time.time() * 1000))) + '.jpg'
        out_img_savedir = os.path.join(img_parent_dir, img_name)
        if not os.path.exists(out_img_savedir):
            os.makedirs(out_img_savedir)

        # Load words from dictionary
        fp_dictionary = set()
        dict_path = './fp_ocr_data/floor-plan-word-dict.txt'
        with open(dict_path, 'r') as f:
            for line in f.readlines():
                fp_dictionary.add(line.strip())
        text_dictionary = list(fp_dictionary)

        ocr_prep_time = time.time()
        text_match_list = []
        for i in range(len(objects_list)):
            bbox = objects_list[i]
            counter += 1
            # scale the bounding box coordinates based on the respective
            # ratios
            startX = int(bbox["xmin"])
            startY = int(bbox["ymin"])
            endX = int(bbox["xmax"])
            endY = int(bbox["ymax"])

            # TODO: make sure correct image format and cordinates reach here
            # crop the box
            cropped = orig_gray[startY:endY, startX:endX]
            # Increase image resolution
            crop_h, crop_w = cropped.shape
            if crop_h * crop_w < 12000:
                try:
                    cropped = cv2.resize(cropped, None, fx=3, fy=3, interpolation=cv2.INTER_CUBIC)
                    crop_h, crop_w = cropped.shape
                except Exception as e:
                    print("caught exception in crop ", cropped)

            # save the bboxes cropped
            img_savepath = os.path.join(out_img_savedir, (str(counter) + '_bbox.jpg'))
            cv2.imwrite(img_savepath, cropped)

            #Rotate the vertical oriented text using aspect ratio
            aspect_ratio = crop_w / crop_h

            # ocr tesseract
            try:
                text = self.print_text_from_image(cropped)
                text_match, match_score = self.get_correct_text(text, text_dictionary)
                text_match_list.append(text_match)
            except Exception as e:
                text_match = text = '##FAILED TESSERACT##'
                match_score = 0

            box_str = '\n-------bbox {}------\n'.format(counter) + \
                      'cordinates: [{}, {}, {}, {}]\n'.format(startX, startY, endX, endY) + \
                      'text: {}\n'.format(text) + \
                      'text_match: {}\n'.format(text_match) + \
                      'match_score: {}\n'.format(match_score) + \
                      '\n_____end_____\n'
            file_str = file_str + box_str
            print(box_str)

            bbox['box'] = counter
            bbox['text'] = str(text)
            bbox['text_match'] = str(text_match)
            bbox['match_score'] = str(match_score)

            if aspect_ratio < 0.75:
                print('Vertical oriented text')
                rot1 = cv2.rotate(cropped, rotateCode=cv2.ROTATE_90_CLOCKWISE)
                rot2 = cv2.rotate(cropped, rotateCode=cv2.ROTATE_90_COUNTERCLOCKWISE)
                text1 = self.print_text_from_image(rot1)
                text2 = self.print_text_from_image(rot2)
                text_match1, match_score1 = self.get_correct_text(text1, text_dictionary)
                text_match2, match_score2 = self.get_correct_text(text2, text_dictionary)
                bbox['rotation1'] = str(text_match1)
                bbox['rotation2'] = str(text_match2)


            # # Add text to word dictionary
            # word_to_add = './fp_ocr_data/word-to-add-raw.txt'
            # word_list = text.split(' ')
            # with open(word_to_add, 'a') as f:
            #     for word in word_list:
            #         f.write(word+'\n')
            #
            # # Add phrase to dictionary
            # phrase_to_add = './fp_ocr_data/phrase-to-add-raw.txt'
            # with open(phrase_to_add, 'a') as f:
            #     f.write(text+'\n')

        ocr_completion_time = time.time()
        img_savepath = os.path.join(out_img_savedir, img_name)
        text_savepath = img_savepath + '.txt'
        with open(text_savepath, 'w+') as f:
            f.write(file_str)

        taxonomy_dict_file = 'fp_ocr_data/inscene-taxonomy-dict.txt'
        taxonomy_dict = self.generate_taxonomy_dict(taxonomy_dict_file)

        json_return = {}
        json_return['ocr'] = objects_list

        summary_dict = {}
        bed_count, bath_count, category_list = self.taxonomy_mapping(text_match_list, taxonomy_dict)
        summary_dict['bed'] = bed_count
        summary_dict['bath'] = bath_count
        summary_dict['categories'] = category_list

        json_return['summary'] = summary_dict

        print('Total time for box detection: ', box_detection_time - start_time)
        print('Time for ocr preparation: ', ocr_prep_time - box_detection_time)
        print('Total time for ocr completion for all bbox: ', ocr_completion_time - ocr_prep_time)
        print('Total time for execution: ', time.time() - start_time)
        return file_name, json_return