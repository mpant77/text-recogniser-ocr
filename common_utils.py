import os
from cv2 import COLOR_BGR2RGB, IMREAD_GRAYSCALE, INTER_LANCZOS4
from cv2 import imread, resize,  cvtColor, Laplacian, convertScaleAbs, equalizeHist, addWeighted, GaussianBlur
from datetime import datetime
from functools import wraps
from hashlib import sha1
from imagehash import phash
from os import listdir, remove, uname, getcwd
from random import randint
import requests
from subprocess import check_output, CalledProcessError
from sys import stderr
from time import strftime, gmtime
from wand.image import Image as wimage
from warnings import simplefilter, warn
import argparse
from urllib import request as Request#, urlopen

import cairosvg
from PIL import Image
from configparser import ConfigParser, ExtendedInterpolation
from numpy import max, array, ones
from pandas import read_csv, read_pickle, DataFrame, Index, merge
from scipy.misc import imresize, imsave
from setuptools.archive_util import unpack_archive
from logs_utils import get_default_logger
from email_utils import send_mail


PROCESSED_EXTENSION = 'processed'
UNPROCESSED_EXTENSION = 'unprocessed'
FINISHED_EXTENSION = 'final'
FINISHED_EXTENSION_old = 'final2'


def deprecated(func):
    """
    Use this decorator to mark functions as deprecated.
    Whenever the deprecated function is called, a warning is emitted

    Arguments:
    :param func: given function

    :return: function with warning
    :type func: object
    """

    @wraps(func)
    def inner(*args, **kwargs):
        simplefilter('always', DeprecationWarning)
        warn("Call to deprecated function {}.".format(func.__name__), category=DeprecationWarning, stacklevel=2)
        simplefilter('default', DeprecationWarning)
        return func(*args, **kwargs)

    return inner


def set_difference(x, y):
    """
    Compute set difference between two data frames by row

    Arguments:
    :param x: data frame
    :param y: data frame

    :return: a data frame containing x - y rows
    :type x: pandas.DataFrame
    :type y: pandas.DataFrame
    """

    set_diff = list(Index(x).difference(Index(y)))
    return DataFrame.from_records(set_diff, columns=x.axes[1])


def get_chunks(n, iterable):
    """
    Obtain a batch of size n from any kind of iterable that allows indexing

    Arguments:
    :param n: batch size
    :param iterable: data on which iteration to be performed

    :return: a batch of size n from iterable
    :type n: int
    :type iterable: iterable
    """
    for pos in range(0, len(iterable), n):
        yield iterable[pos:pos + n]


def output_columns(k=2):
    """
    obtain the csv file header for output

    Arguments:
    :param k: top k labels

    :return: top line of csv file
    :type k: int
    """
    first_row = ['ImageID', 'ImageName', 'Path', 'Url']
    # first_row = 'Image_id,Image_name,Path'
    for i in range(k):
        first_row.extend([''.join(['Label', str(i + 1)]), ''.join(['Prob', str(i + 1)])])
        # first_row += ',' + 'Label-' + str(i + 1) + ',' + 'Prob-' + str(i + 1)
    # first_row += ',Height,Width,PPIx,PPIy,Color,Blur'
    first_row.extend(['Height', 'Width', 'PPIx', 'PPIy', 'Color', 'ImageQuality', 'ImageQualityScale', 'Fingerprint', 'Phash'])
    first_row.extend(['MachineInfo', 'MachineDate', 'NumModels', 'ModelVersions', 'Generation'])
    first_row.extend(['IsVerified', 'VerifiedBy', 'VerifiedDate'])
    first_row.extend(['IsLineDrawing', 'IsBlur', 'IsConstruction', 'Category', 'ObjectDetection', 'ConstructionObjects', 'SubLabels', 'Shotsize', 'ShotAngle', 'DominantColors'])  

    return first_row


def output_columns_old(k=2):
    """
    obtain the csv file header for output

    Arguments:
    :param k: top k labels

    :return: top line of csv file
    :type k: int
    """
    first_row = ['ImageID', 'ImageName', 'Path', 'Url']
    # first_row = 'Image_id,Image_name,Path'
    for i in range(k):
        first_row.extend([''.join(['Label', str(i + 1)]), ''.join(['Prob', str(i + 1)])])
        # first_row += ',' + 'Label-' + str(i + 1) + ',' + 'Prob-' + str(i + 1)
    # first_row += ',Height,Width,PPIx,PPIy,Color,Blur'
    first_row.extend(['Height', 'Width', 'PPIx', 'PPIy', 'Color', 'IsBlur', 'ImageQuality', 'Fingerprint'])
    first_row.extend(['MachineInfo', 'MachineDate', 'NumModels', 'ModelVersions'])
    first_row.extend(['IsVerified', 'VerifiedBy', 'VerifiedDate'])
    first_row.extend(['IsLineDrawing', 'Category'])

    return first_row


def get_config_option(config, section, key, default_value):
    """
    Obtain a value from config file object for a given section and key

    Arguments:
    :param config: config file object
    :param section: section name
    :param key: key inside given section name
    :param default_value: default value of the key

    :return: returns config option value
    :type config: ConfigParser
    :type section: str
    :type key: str
    :type default_value: object
    """
    ret = default_value
    if config.has_option(section, key):
        if isinstance(default_value, bool):
            ret = config.getboolean(section, key)
        elif isinstance(default_value, int):
            ret = config.getint(section, key)
        elif isinstance(default_value, float):
            ret = config.getfloat(section, key)
        else:
            ret = config.get(section, key)
    return ret


def get_run_state(files):
    """
    Obtain program run state

    Arguments:
    :param files: inverted index of file names by extension

    :return: a integer value indicating program run state
    :type files: dict
    """
    run_state = 0  # running for first time
    if FINISHED_EXTENSION in files:
        run_state = 4
    elif UNPROCESSED_EXTENSION in files:
        run_state += 1  # started but no output generated yet
        if PROCESSED_EXTENSION in files:
            run_state += 1  # partial output generated
    elif PROCESSED_EXTENSION in files:
        run_state = 3

    return run_state


def get_timestamp(milliseconds=False):
    """
    Obtain current timestamp

    Keyword Arguments
    :param milliseconds: whether to report time in milliseconds or not

    :return: timestamp in string format
    :type milliseconds: bool
    """
    time_stamp = datetime.utcnow().isoformat().split('.')
    ret = time_stamp[0]
    if milliseconds:
        ret += '.' + time_stamp[1][:3]
    return ret


def is_valid_file(file_name):
    """
    Checks if given file name is valid file of size at-least 1Kb

    Arguments:
    :param file_name:

    :return: file validity status
    """

    file_exists = os.path.isfile(file_name)
    if file_exists:
        if os.stat(file_name).st_size > 1023:
            return True
    return False


class FileUtils(object):

    def __init__(self):
        pass

    @staticmethod
    def get_file_hash(file_name, hash_function=sha1):
        """
        Obtain file hash

        Arguments:
        :param file_name: full path of file
        :param hash_function: hash function

        :return: returns file hash as hexadecimal string
        :type file_name: str
        :type hash_function: valid hash function
        """
        with open(file_name, 'rb') as f:
            hash_object = hash_function(f.read())

        return hash_object.hexdigest()

    @staticmethod
    def get_PIL_Image_opencv(image):
        """
        Convert Image array of Opencv to PIL Image array

        Arguments:
        :param image: input image array (cv2)

        :return:
        :type image: PIL Image format array
        """
        cv2_im = cvtColor(image, COLOR_BGR2RGB)
        pil_im = Image.fromarray(cv2_im)
        return pil_im

    @staticmethod
    def index_files_by_extension(file_names):
        """
        Obtain inverted index of file names by extension
        Arguments:
        :param file_names: file names or full file path

        :return: a dictionary with file extension as key and file name as value
        :type file_names: list
        """
        files = {}
        for local_file in file_names:
            extension = local_file.rsplit('.', 1)[-1]
            if extension not in files:
                files[extension] = []
            files[extension].append(local_file)
        return files

    @classmethod
    def get_files_by_extension(cls, directory):
        """
        Obtain an inverted index of all file names by extension in current directory

        Arguments:
        :param directory: root directory

        :return: a dictionary of inverted file index by extension
        :type directory: str
        """
        files = cls.index_files_by_extension(listdir(directory))
        for ext, file_list in files.items():
            files[ext] = [os.path.join(directory, file_name) for file_name in file_list]
        return files

    @staticmethod
    def get_modified_name(file_name):
        """
        Generate time stamp based file name prefix

        Arguments:
        :param file_name:  original input file name

        :return: File name containing utc time stamp
        :type file_name: str
        """
        return '-'.join([file_name.rsplit('.', 1)[0], get_timestamp().replace(':', '-')])

    @staticmethod
    def extract_model(archive):
        """
        Extract all components of model from tar gzipped archive

        Arguments:
        :param archive: full path of the archive

        :return: a dictionary of model components
        :type archive: str
        """
        tmp_dir = os.path.join('/tmp/', archive.split('.')[0])
        if not os.path.isdir(tmp_dir):
            os.mkdir(tmp_dir)
            unpack_archive(archive, tmp_dir)
        keys = ['model', 'mean', 'deploy', 'labels']
        parameters = dict()
        for file_name in os.listdir(tmp_dir):
            file_path = os.path.join(tmp_dir, file_name)
            if file_name.endswith('.caffemodel'):
                parameters['model'] = file_path
            elif file_name.endswith('.binaryproto'):
                parameters['mean'] = file_path
            elif file_name == 'deploy.prototxt':
                parameters['deploy'] = file_path
            elif file_name == 'labels.txt':
                parameters['labels'] = file_path

        parameters['name'] = archive.split('.')[0]

        for param in keys:
            assert param in parameters, '%s is missing from archive, tmp dir is %s' % (param, tmp_dir)
        return parameters


class ImageUtils(object):

    def __init__(self):
        pass

    @classmethod
    def get_image_from_url(cls, image_url, full_image_path):
        """
        Download an image from a url

        Arguments:
        :param image_url: http url pointing to an image
        :param full_image_path: local file path where image is saved

        :return: status code indicating whether image existed, was downloaded or error occurred
        :type image_url: str
        :type full_image_path: str
        """
        python_status = None
        curl_status = None
        ret, wget_status = cls.download_image(image_url, full_image_path, method='wget')
        if ret > 1:
            ret, python_status = cls.download_image(image_url, full_image_path, method='python')
        # if ret > 1:
        #     ret, curl_status = cls.download_image(image_url, full_image_path, method='curl')
        return ret, "wget-{} python-{} curl-{}".format(wget_status, python_status, curl_status)

    @staticmethod
    def to_png(full_image_path):
        """
        Convert a given image path to a png image

        Arguments:
        :param full_image_path: local file path where image is saved

        :return: a png image  in PIL format
        :type full_image_path: str
        """

        image_name = full_image_path.split('.')[0] + '.png'
        tmp_img = None
        try:
            if not os.path.isfile(image_name):
                if full_image_path.endswith('svg'):
                    with open(full_image_path, 'r') as svg_file:
                        svg_xml = ''.join([line for line in svg_file if len(line) > 0])
                        with open(image_name, 'w') as png_file:
                            cairosvg.svg2png(bytestring=svg_xml, write_to=png_file)
                    tmp_img = Image.open(image_name)
                else:
                    tmp_img = Image.open(full_image_path)
                    tmp_img = tmp_img.convert('RGB')
                    tmp_img.save(image_name, 'PNG')
            else:
                return Image.open(image_name)

        except Exception as ex:
            logger = get_default_logger()
            logger.critical(ex, '\n%s cannot be converted' % full_image_path)
        return tmp_img

    @staticmethod
    def correct_image_meta_type(meta_type):
        """
        Verify and return valid image color type

        Arguments:
        :param meta_type:

        :return: valid image color type
        :type meta_type: str
        """
        meta_type_val = meta_type
        # make sure image type isn't a list
        if isinstance(meta_type, list):
            meta_type_val = ' '.join(meta_type)
        # make sure image type is within allowed class
        allowed_types = {'Bilevel', 'Grayscale', 'GrayscaleMatte', 'Palette', 'PaletteMatte', 'TrueColor',
                         'TrueColorMatte', 'ColorSeparation', 'ColorSeparationMatte'}
        if meta_type_val not in allowed_types:
            res = 'TrueColor'  # default type
            for item in allowed_types:
                if meta_type_val.find(item) != -1:
                    # found correct type
                    res = item
                    break
            meta_type_val = res
        return meta_type_val

    @staticmethod
    def is_image_blur(full_image_path, threshold=100):
        """
        Check whether an image is blur or not

        Arguments:
        :param full_image_path: local image path

        Keyword arguments:
        :param threshold: determine numerical threshold for image blur

        :return: binary value indicating image is blur(True) or not (False)
        :type full_image_path: str
        :type threshold: int
        """
        blur_flag = 0
        try:
            gray_image = imread(full_image_path, IMREAD_GRAYSCALE)
            if gray_image is not None:
                focus_measure = max(convertScaleAbs(Laplacian(gray_image, 3)))
                if focus_measure < threshold:
                    blur_flag = 1
        except Exception as ex:
            logger = get_default_logger()
            logger.error(ex)
        return blur_flag

    @classmethod
    def get_image_metadata(cls, full_image_path):
        """
        Extract image metadata from image path
        Requires image magick to be installed

        Arguments:
        :param full_image_path: local image path

        :return: a dictionary of image metadata
        :type full_image_path: str
        """
        meta = dict()
        try:
            metadata = check_output(["identify", "-format", "%h %w %x %y %U %[type]", full_image_path]).decode("utf-8") .split(' ')
            meta['Height'] = metadata[0]
            meta['Width'] = metadata[1]
            meta['PPIx'] = float(metadata[2])
            meta['PPIy'] = float(metadata[3])
            if metadata[4] == 'PixelsPerCentimeter':
                meta['PPIx'] *= 2.54
                meta['PPIy'] *= 2.54
            meta['PPIx'] = int(round(meta['PPIx'], 0))
            meta['PPIy'] = int(round(meta['PPIy'], 0))
            meta['Color'] = cls.correct_image_meta_type(' '.join(metadata[5:]))
            meta['IsBlur'] = cls.is_image_blur(full_image_path)
            meta['Fingerprint'] = FileUtils.get_file_hash(full_image_path)
        except Exception as ex:
            logger = get_default_logger()
            logger.critical(ex, '\n%s metadata not extracted' % full_image_path)
            meta = dict()

        return meta

    @classmethod
    def image_metadata(cls, full_image_path):
        """
        Extract image metadata from image path

        Arguments:
        :param full_image_path: local image path

        :return: a dictionary of image metadata
        :type full_image_path: str
        """
        meta = dict()
        try:
            with wimage(filename=full_image_path) as image:
                meta['Height'] = image.height
                meta['Width'] = image.width
                meta['PPIx'] = image.resolution[0]
                meta['PPIy'] = image.resolution[1]
                meta['Color'] = image.type
            meta['IsBlur'] = cls.is_image_blur(full_image_path)
            meta['Fingerprint'] = FileUtils.get_file_hash(full_image_path)
        except Exception as ex:
            logger = get_default_logger()
            logger.critical(ex, '\n%s metadata not extracted' % full_image_path)
            meta = dict()

        return meta

    @classmethod
    def read_image(cls, full_image_path):
        """
        Read image from disk

        Arguments:
        :param full_image_path: local image path

        :return: returns an image array in  Opencv or PIL format
        :type full_image_path: str
        """
        try:
            if full_image_path.endswith(('gif', 'svg')):
                cls.to_png(full_image_path)
                full_image_path = full_image_path.split('.')[0] + '.png'
            image_read = imread(full_image_path)
            if image_read is None:
                image_read = array(Image.open(full_image_path).convert('RGB'))[:, :, ::-1]
            return image_read #imread(full_image_path)
        except Exception as ex:
            logger = get_default_logger()
            logger.critical(full_image_path, 'not processed\n' + str(ex))

    @staticmethod
    def resize_image(image, use_scipy=False, height=299, width=299):
        """
        Resize an image with lanczos interpolation

        Arguments:
        :param image: input image array

        Keyword arguments:
        :param height: resize image height
        :param width:  resize image width
        :param use_scipy: whether to use scipy for image resizing

        :return:
        :type image: array
        :type height: int
        :type width: int
        :type use_scipy: bool
        """
        try:
            if not use_scipy:
                image = cvtColor(image, COLOR_BGR2RGB)
                #image = array(image)
                image = image / 255.0
                image = resize(image, (height, width))#, interpolation=INTER_LANCZOS4)
                #image = cvtColor(image, COLOR_BGR2RGB)
                return image
            else:
                image = imresize(image, (height, width), interp='lanczos')
                return image

        except Exception as ex:
            logger = get_default_logger()
            logger.critical('Image cannot be resized ' + str(ex))

    @staticmethod
    def get_imagehash(image, hashalgo=phash):
        """
        Obtain phash of an image

        Arguments:
        :param image: input image array (cv2)

        Keyword arguments:
        :param hashalgo: phash algorithm

        :return:
        :type str: hash of image
        """
        pil_image = FileUtils.get_PIL_Image_opencv(image)
        return str(phash(pil_image))

    @staticmethod
    def image_histogram_equalization(image):
        """
        Perform histogram equalization on given image

        Arguments:
        :param image: input image

        :return: an image with histogram equalized
        :type image: array
        """
        image[:, :, 0] = equalizeHist(image[:, :, 0])
        image[:, :, 1] = equalizeHist(image[:, :, 1])
        image[:, :, 2] = equalizeHist(image[:, :, 2])
        return image

    @staticmethod
    def sharpen_image(image, kernel=(5, 5), opacity=1.5):
        """takes an image as input and returns the sharpened image

        Arguments:
        :param image image

        Keyword arguments:
        :param kernel: size of convolution kernel
        :param opacity: value  of opacity

        :return sharpened image
        :type image: uint8
        :type kernel: tuple
        :type opacity: int

        """
        gaussian_blur = GaussianBlur(image, kernel, 0)
        image = addWeighted(image, opacity, gaussian_blur, 1.0 - opacity, 0, image)
        return image

    @staticmethod
    @deprecated
    def scipy_to_opencv(image):
        """
        Convert scipy image to opencv format

        Arguments:
        :param image: input image

        :return: opencv image
        :type image: PIL.Image
        """

        random_image_path = '/tmp/' + FileUtils.get_modified_name(randint(1, 1000)) + '.jpg'
        imsave(random_image_path, image)
        new_image = imread(random_image_path)
        new_image = cvtColor(new_image, COLOR_BGR2RGB)
        remove(random_image_path)
        return new_image

    @staticmethod
    def download_image(image_url, full_image_path, method="curl"):
        status = None
        logger = get_default_logger()
        ret = 0
        if not is_valid_file(full_image_path):
            if method == "wget":
                command = ["wget", "-U", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)", "--connect-timeout=3", "--tries=1", "-nc", "-q", image_url, "-O", full_image_path]
                try:
                    with open(os.devnull, 'w') as FILE_NULL:
                        check_output(command, stderr=FILE_NULL)
                        ret = 1
                except CalledProcessError as e:
                    status = e.returncode
                    logger.critical(e)
                    ret = 2
            elif method == "curl":
                command = ["curl",  "--connect-timeout", "1","--retry", "1", "-A", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)", "-o", full_image_path, image_url]
                status_check = ["curl", "--connect-timeout", "3","--retry", "1", "-A", "Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)", "-s", "-L", "-I", '-w "%{http_code}"', image_url, "-o", "/dev/null"]
                try:
                    with open(os.devnull, 'w') as FILE_NULL:
                        status = check_output(status_check, stderr=FILE_NULL).decode("utf-8").strip().strip('"')
                    if status == "200":
                        with open(os.devnull, 'w') as FILE_NULL:
                            check_output(command, stderr=FILE_NULL)
                        ret = 1
                    else:
                        logger.error('Image_url could not be downloaded via curl {} - {}'.format(image_url, status))
                        ret = 2
                except Exception as e:
                    logger.critical(e)
                    ret = 2
            elif method == "python":
                try:
                    headers = {'User-Agent': 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'}
                    response = requests.get(image_url, headers=headers, timeout=1)
                    status_code = response.status_code # Response Code  
                    if status_code == 200:
                        with open(full_image_path, 'wb') as f:
                            for chunk in response:
                                f.write(chunk)
                        ret = 1
                    else:
                        status = status_code
                        ret = 2
                except Exception as e:
                    logger.critical(e)
                    ret = 2
        return ret, status


def get_inference_parameters(config_file_name):
    """
    Obtain inference parameters from input config file

    Arguments:
    :param config_file_name: input config file name

    :return: All relevant parameters needed for inference
    :type config_file_name: str
    """

    params = {'models': [], 'versions': []}
    config = ConfigParser()
    config._interpolation = ExtendedInterpolation()
    config.read(config_file_name)

    section = 'MISC'
    params['batch'] = get_config_option(config, section, 'batch_size', 10)
    params['generation'] = get_config_option(config, section, 'generation', 'Gen5')
    params['gpu'] = get_config_option(config, section, 'use_gpu', True)
    params['k'] = get_config_option(config, section, 'k', 2)
    params['cols'] = get_config_option(config, section, 'cols', 'ImageID\nUrl').split()
    params['unsharp'] = get_config_option(config, section, 'unsharp', False)
    params['equalize'] = get_config_option(config, section, 'equalize', False)
    params['save'] = get_config_option(config, section, 'save', False)
    params['shape'] = get_config_option(config, section, 'shape', 299)  # for TF 299, for caffe 224
    params['cache'] = get_config_option(config, section, 'cache', '/tmp/images')
    params['download-batch'] = get_config_option(config, section, 'download-batch', 100)
    params['use-cache'] = get_config_option(config, section, 'use-cache', True)
    params['from'] = get_config_option(config, section, 'from', '')
    params['to'] = get_config_option(config, section, 'to', '').split('\n')
    params['secret-file'] = get_config_option(config, section, 'secret-file', '')
    params['credentials'] = get_config_option(config, section, 'credentials', 'credentials.storage')

    """section = 'CAFFE'
    archives_location = config.get(section, 'archives').split()
    for archive in archives_location:
        assert os.path.isfile(archive), 'archive %s does not exist' % archive
        params['models'].append(FileUtils.extract_model(archive))
        params['versions'].append(str(archive.rsplit('/')[-1].split('.')[0]))

    section = 'DRAWING'
    params['drawing'] = []
    line_drawing_archives = config.get(section, 'drawing').split()
    for archive in line_drawing_archives:
        assert os.path.isfile(archive), 'archive %s does not exist' % archive
        params['drawing'].append(FileUtils.extract_model(archive))

    section = 'SUBLABELS'
    for sublabel in ['amenities', 'bathroom', 'bedroom', 'floorplan', 'exteriorothers', 'water', 'construction']:
        params[sublabel] = []
        sublabel_archive = config.get(section, sublabel).split()
        for archive in sublabel_archive:
            assert os.path.isfile(archive), 'archive %s does not exist' % archive
            params[sublabel].append(FileUtils.extract_model(archive))
    """
    section = 'TENSORFLOW'
    params['TF_main'] = get_config_option(config, section, 'main', "")
    params['TF_amenities'] = get_config_option(config, section, 'amenities', "")
    params['TF_bathroom'] = get_config_option(config, section, 'bathroom', "")
    params['TF_bedroom'] = get_config_option(config, section, 'bedroom', "")
    params['TF_floorplan'] = get_config_option(config, section, 'floorplan', "")
    params['TF_exteriorothers'] = get_config_option(config, section, 'exteriorothers', "")
    params['TF_water'] = get_config_option(config, section, 'water', "")
    params['TF_construction'] = get_config_option(config, section, 'construction', "")
    params['TF_linedrawing'] = get_config_option(config, section, 'linedrawing', "")
    return params


def get_unprocessed(files, run_state, use_columns):
    """
    Extract  updated unprocessed records

    Arguments:
    :param files: inverted index of file names by extension
    :param run_state: state of program execution
    :param use_columns: columns names used for processing

    :return:  pandas DataFrame containing updated unprocessed records
    :type files: dict
    :type run_state: int
    :type use_columns: list
    """
    if run_state == 0:
        input_file = files['csv'][0]
        unprocessed = read_csv(input_file, usecols=use_columns, error_bad_lines=False)
    elif run_state == 1:
        unprocessed = read_pickle(files[UNPROCESSED_EXTENSION][0])
    elif run_state == 2:
        processed = read_csv(files[PROCESSED_EXTENSION][0], sep='|',usecols=use_columns, error_bad_lines=False)
        unprocessed = read_pickle(files[UNPROCESSED_EXTENSION][0])

    if run_state < 2:
        processed = DataFrame(columns=use_columns)

    return set_difference(unprocessed, processed)


def generate_old_output_file(filename, output_file_name):
    read_csv(filename, sep='|', error_bad_lines=False)[output_columns_old()].to_csv(output_file_name, index=False)


def unprocessed_set(files, run_state, use_columns):
    """
    Update stored unprocessed file and return the updated records

    Arguments:
    :param files: inverted index of file names by extension
    :param run_state: state of program execution
    :param use_columns: columns names used for processing

    :return: pandas DataFrame containing updated unprocessed records
    :type files: dict
    :type run_state: int
    :type use_columns: list
    """
    if UNPROCESSED_EXTENSION in files:
        files[UNPROCESSED_EXTENSION] = sorted(files[UNPROCESSED_EXTENSION], reverse=True)  # lifo order
    if PROCESSED_EXTENSION in files:
        files[PROCESSED_EXTENSION] = sorted(files[PROCESSED_EXTENSION], reverse=True)  # lifo order
    unprocessed = get_unprocessed(files, run_state, use_columns)
    input_file = files['csv'][0]
    out_file_name = FileUtils.get_modified_name(input_file) + '.' + UNPROCESSED_EXTENSION
    if UNPROCESSED_EXTENSION in files and len(files[UNPROCESSED_EXTENSION]) > 1:
        remove(files[UNPROCESSED_EXTENSION][-1])  # remove the oldest file
    unprocessed.to_pickle(out_file_name)

    return unprocessed


def estimated_time_remaining(images_processed, time_spent, remaining_images):
    """
    Obtain approximate estimate of completion of the process

    Arguments:
    :param images_processed: number of images processed
    :param time_spent: time spent in computation of current batch
    :param remaining_images: Number of unprocessed images

    :return: number of seconds for process to finish
    :type images_processed: int
    :type time_spent: float
    :type remaining_images: int
    """
    if time_spent < 1e-8:
        time_spent = 1e-8
    rate_of_process = 1.0 * images_processed / time_spent
    if rate_of_process > 1e6 or rate_of_process < 1e-8:
        rate_of_process = 1e3
    return remaining_images * 1.0 / rate_of_process


def add_static_fields(results, path):
    """
    Add static columns with default values to data frame

    Arguments:
    :param results: data frame with image properties
    :param path: local path of images storage

    :return: a data frame containing new static columns along with existing one's
    :type results: DataFrame
    :type path: local image path
    """
    static = {'Path': path, 'ImageQuality': -1, 'ImageQualityScale': 'NA',
              'MachineInfo': '_'.join([uname()[i] for i in [0, 1, 2, 4]]),
              'MachineDate': strftime("%Y-%m-%d %H:%M:%S", gmtime()),
              'IsVerified': 0, 'VerifiedBy': '', 'VerifiedDate': '',
              'Shotsize': '{}', 'ShotAngle' : '{}', 'DominantColors' : '[]', 'NumModels': 1, 'ModelVersions' :'TF'
              }
    for key, value in static.items():
        results[key] = value
    if 'Label2' not in results:
        results['Label2'] = ""
        results['Prob2'] = 0
    if 'Label1' in results:
        results['Category'] = results['Label1']
    return results


def transform_predictions(predictions, k=2):
    """
    Create a data frame of image predictions

    Arguments:
    :param predictions: text label and probabilities for given images
    :param k: top-k classes

    :return: a data frame of image predictions
    :type predictions: dict
    :type k: int
    """
    image_predictions = dict()
    for image, prediction in predictions.items():
        pred = dict()
        for index, label_prob in enumerate(prediction):
            if index < k:
                pred['Label' + str(index + 1)] = label_prob[0]
                pred['Prob' + str(index + 1)] = round(label_prob[1],2)
            else:
                break
        image_predictions[image] = pred
    # convert dict to data frame
    predictions_frame = DataFrame(image_predictions).T
    predictions_frame.index.name = 'ImageName'
    predictions_frame.reset_index(level=0, inplace=True)
    return predictions_frame


def merge_properties(predictions, records, metadata, cache_location):
    """
    Combine various image properties to create a final output data frame

    Arguments:
    :param predictions: image predictions
    :param records: input data records
    :param metadata: image metadata
    :param cache_location: downloaded image files are saved at this location

    :return: a final data frame of image properties
    :type predictions: DataFrame
    :type records: DataFrame
    :type metadata: DataFrame
    :type cache_location: str
    """

    selected_columns = list(set(metadata.columns).difference(set(['resize_image', 'Height', 'Width'])))
    result_i = merge(predictions, metadata[selected_columns], on=['ImageName'])
    result_ii = merge(records, result_i, how='left').dropna(axis=0, subset=['Label1'])
    result_iii = add_static_fields(result_ii, cache_location)
    final = result_iii[output_columns()]  # reorder column names
    return final


def get_ftp_options():
    """
    Obtain base parser that supports reading ftp options as command line arguments
    :return: argument parser
    """
    parent = argparse.ArgumentParser(add_help=False)
    parent.add_argument('--host', metavar='hostname', type=str, default=None, help='FTP host name (default: None)')
    parent.add_argument('--down', metavar='path', type=str, default=None, help='FTP download directory (default: None)')
    parent.add_argument('--up', metavar='path', type=str, default=None, help='FTP upload directory (default: None)')
    child_parser = argparse.ArgumentParser(parents=[parent])

    return child_parser


def get_unprocessed_input_files(input_file_names, output_file_names):
    input_file_names = array(input_file_names)
    output_file_names = array(output_file_names)
    not_found = ones(input_file_names.size, dtype=bool)

    for i in range(len(input_file_names)):
        input_file_name = input_file_names[i].rsplit('.', 1)[0]
        for j in range(len(output_file_names)):
            if input_file_name in output_file_names[j]:
                not_found[i] = False
                break

    return list(input_file_names[not_found])


def get_remote_input_file(ftp, remote_dir, policy='size_asc'):

    files = FileUtils.index_files_by_extension(ftp.list_directory(remote_dir))
    extension = 'csv'
    selected_file_name = ''
    if extension in files:
        selected_file_name = files[extension][0]
        if len(files[extension]) > 1:
            curr_size = 1023
            if policy == 'size_asc':
                curr_size = int(1e9)
            for file_name in files[extension]:
                file_size = ftp.size(remote_dir + '/' + file_name)
                if policy == 'size_asc' and file_size < curr_size:
                    curr_size = file_size
                    selected_file_name = file_name
                elif policy == 'size_desc' and file_size > curr_size:
                    curr_size = file_size
                    selected_file_name = file_name

    return selected_file_name


def get_local_input_file():

    files = FileUtils.get_files_by_extension(getcwd())
    selected_file_name = ''
    if 'csv' in files and len(files['csv']) > 0:
        selected_file_name = files['csv'][0]
    return selected_file_name


def exist_ftp(selected_file_path, files):

    selected_file_name = selected_file_path.rsplit('/', 1)[-1]
    if 'csv' in files:
        for file_name in files['csv']:
            if file_name == selected_file_name:
                return True
    if 'csv_process' in files:
        for file_name in files['csv_process']:
            if file_name.find(selected_file_name) == 0:  # file name must be begin with same name
                return True

    return False


def create_unprocessed_records_frame(error_file_name):
    error_log_file_path = error_file_name
    dt = read_csv(error_log_file_path, sep='|', header=None, names=['Log'])
    dt = dt[dt.Log.str.contains("ERROR - ImageClassifierError - ")].Log.values.tolist()
    frame = DataFrame()
    for record in dt:
        temp_frame = DataFrame(record[record.find("ERROR - ImageClassifierError - ") + 31:].split(",")).T
        temp_frame.columns= ['Error', 'Url', 'Local_path']
        frame = frame.append(temp_frame)
    if len(frame) > 0:
        frame = frame.drop_duplicates('Url', keep='first')
    return frame
    

def create_summary(input_file_path, output_file_path, error_file_name, url_col='Url'):

    inp = read_csv(input_file_path, error_bad_lines=False)
    output = read_csv(output_file_path, error_bad_lines=False, sep='|')

    def shape(frame, cols, duplicate=False):
        df = frame
        if duplicate:
            df = df.drop_duplicates(subset=cols)
        return df.shape[0]

    unprocessed_urls = set_difference(inp[[url_col]].drop_duplicates(), output[[url_col]].drop_duplicates())
    records = [(shape(inp, url_col), shape(output, url_col), shape(inp, url_col, duplicate=True),
                shape(output, url_col, duplicate=True), len(unprocessed_urls))]

    summary = DataFrame(records,
                        columns=['Input   ', 'Output  ',
                                 'Unique  ', 'Done    ', 'Unprocessed'])
    return tuple([summary, create_unprocessed_records_frame(error_file_name)])


def send_daily_summary(input_file_path, output_file_path, params, ftp_download_path, error_file_name):
    summary, failed_urls = create_summary(input_file_path, output_file_path, error_file_name)
    out_file_name = '/tmp/' + params['input-file'] + '-unprocessed-urls.csv'
    with open(out_file_name, 'w') as f:
        failed_urls.to_csv(f, index=False)
        f.flush()
    time_stamp = datetime.utcnow().strftime("%b-%d-%Y")
    #subject = ' '.join([time_stamp, 'Image Classification Summary for ', params['input-file']])
    if "Prod" in ftp_download_path:
        subject = ' '.join([params['generation'], 'TFProd:', time_stamp, ' Image Classification Summary for ', params['input-file']])
    elif "Test" in ftp_download_path:
        subject = ' '.join([params['generation'], 'Test:', time_stamp, ' Image Classification Summary for ', params['input-file']])
    elif "Sprint" in ftp_download_path:
        subject = ' '.join([params['generation'], 'Sprint:', time_stamp, ' Image Classification Summary for ', params['input-file']])
    elif "Stage" in ftp_download_path:
        subject = ' '.join([params['generation'], 'Stage:', time_stamp, ' Image Classification Summary for ', params['input-file']])

    if len(params['to']) > 0 and params['secret-file'] != '':
        send_mail(params['from'], params['to'], subject,
                  summary.to_string(index=False, index_names=False, justify='left', col_space=10),
                  params['secret-file'], params['credentials'], out_file_name)
    else:
        logger = get_default_logger()
        logger.error('Could not mail the following summary\n{}'.format(summary.to_string()))
